/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { createStore, applyMiddleware, combineReducers } from "redux";
import { Provider, connect } from "react-redux";
import { Scene, Actions, Router } from "react-native-router-flux";
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
  createNavigationReducer
} from "react-navigation-redux-helpers";
import promise from "redux-promise-middleware";
import thunk from "redux-thunk";

import logger from "redux-logger";

import { AppNavigator, ReduxRouter } from "./src/config/routes";

import appReducer from "./src/reducers/index";

const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav
);

const ReduxNavigator = reduxifyNavigator(AppNavigator, "root");

export const store = createStore(
  appReducer,
  applyMiddleware(middleware, promise(), thunk)
);

class App extends Component {
  onBackPress = () => {
    if (Actions.state.index === 0) {
      return false;
    }
    Actions.pop();
    return true;
  };

  render() {
    return (
      <Provider store={store}>
        <ReduxRouter
          backAndroidHandler={this.onBackPress}
          navigator={ReduxNavigator}
        />
      </Provider>
    );
  }
}

export default App;

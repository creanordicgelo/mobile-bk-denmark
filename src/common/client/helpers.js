import * as _ from "lodash";
import moment from "moment";
import SInfo from "react-native-sensitive-info";

import { USER_TOKEN, HAS_TOKEN, NO_TOKEN } from "../actions/constants";

export const hasValidToken = token => {
  const { expires_in, last_login_time } = token;
  const expiryDate = moment(+expires_in + last_login_time);
  return !moment().isAfter(expiryDate);
};

export const checkValidToken = value => {
  if (_.isEmpty(value) || _.isUndefined(value)) {
    return null;
  }

  const token = JSON.parse(value);

  // Check for valid token
  if (hasValidToken(token)) {
    return token.access_token;
  }

  return null;
};

export const getValidToken = async noToken => {
  if (_.isUndefined(noToken)) return null;

  return SInfo.getItem(USER_TOKEN, {}).then(value => checkValidToken(value));
};

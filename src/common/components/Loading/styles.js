import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    height: "100%",
    width: "100%",
    position: "absolute",
    alignItems: "center",
    justifyContent: "center"
  },
  imgMenu: {
    width: 80,
    height: 80,
    backgroundColor: "transparent",
    position: "absolute",
    alignSelf: "center"
  }
});

export default styles;

import React from "react";
import { View, Image } from "react-native";

import styles from "./styles";

const Loading = ({ isVisible }) => {
  if (isVisible) {
    return (
      <View style={styles.container}>
        <Image
          style={styles.imgMenu}
          source={require("../../../images/bk-loading.gif")}
        />
      </View>
    );
  } else {
    return null;
  }
};

export default Loading;

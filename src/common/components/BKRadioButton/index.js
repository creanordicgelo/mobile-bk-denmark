import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity } from "react-native";

const BkRadioButton = ({ onActionClick, size, theme, selected }) => (
  <TouchableOpacity
    onPress={() => {
      onActionClick();
    }}
  >
    <View
      style={{
        height: size,
        width: size,
        borderRadius: size / 2,
        borderWidth: 2,
        borderColor: theme,
        alignItems: "center",
        justifyContent: "center"
      }}
    >
      {selected ? (
        <View
          style={{
            height: size / 2,
            width: size / 2,
            borderRadius: size / 4,
            backgroundColor: theme
          }}
        />
      ) : null}
    </View>
  </TouchableOpacity>
);

BkRadioButton.propTypes = {
  onActionClick: PropTypes.func,
  theme: PropTypes.string,
  size: PropTypes.number,
  selected: PropTypes.bool
};

BkRadioButton.defaultProps = {
  onActionClick: () => {},
  theme: "",
  size: 24,
  selected: false
};

export default BkRadioButton;

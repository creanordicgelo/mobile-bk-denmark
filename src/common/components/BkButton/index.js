import React from "react";
import PropTypes from "prop-types";
import { View, Text, Image, TouchableOpacity } from "react-native";

import constants from "../../values";
import textStyles from "../../values/textStyles";

const BkButton = ({ onActionClick, title, theme, fontAttr }) => (
  <TouchableOpacity
    onPress={() => {
      onActionClick();
    }}
  >
    <View>
      <Text
        style={[
          textStyles.txtCalloutStyle,
          { color: theme, fontSize: fontAttr }
        ]}
      >
        {title}
      </Text>

      <View
        style={{
          marginTop: 4,
          height: 4,
          backgroundColor: theme,
          width: "100%"
        }}
      />
    </View>
  </TouchableOpacity>
);

BkButton.propTypes = {
  onActionClick: PropTypes.func,
  title: PropTypes.string,
  theme: PropTypes.string,
  fontAttr: PropTypes.number
};

BkButton.defaultProps = {
  onActionClick: () => {},
  title: "",
  theme: "",
  fontAttr: ""
};

export default BkButton;

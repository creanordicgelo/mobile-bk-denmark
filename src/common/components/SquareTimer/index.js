import React, { useRef, useEffect, useState } from "react";
import { View, Animated } from "react-native";
import { AnimatedSVGPaths } from "react-native-svg-animations";

import T from "./PathTop";
import R from "./PathRight";
import B from "./PathBottom";
import L from "./PathLeft";

export default (HiSVG = ({ duration }) => {
  const [totalDuration, setTotalDuration] = useState(0);

  const timer = duration / 4;
  const totalTime = duration + timer + 400;

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (totalDuration === duration) {
        setTotalDuration(0);
      } else {
        setTotalDuration(duration);
      }
    }, totalTime);

    return () => {
      clearTimeout(timeout);
    };
  }, [totalDuration]);

  const animatedSVGContainer = (color, stroke, delay, svg, addedStyles) => {
    return (
      <View
        style={[
          {
            position: "absolute",
            top: -20,
            left: -20
          },
          addedStyles
        ]}
      >
        <AnimatedSVGPaths
          strokeColor={color}
          strokeWidth={stroke}
          width={1000}
          duration={timer}
          scale={0.55}
          delay={delay}
          ds={svg}
          loop={false}
        />
      </View>
    );
  };

  const renderSquareBorderAnimation = () => {
    return (
      <View>
        {/*TOP BORDER*/}
        {animatedSVGContainer("brown", 18, 900, T)}
        {animatedSVGContainer("white", 20, timer + 400, T)}

        {/*RIGHT BORDER*/}
        {animatedSVGContainer("green", 18, timer + 900, R)}
        {animatedSVGContainer("white", 20, timer * 2 + 400, R)}

        {/*BOTTOM BORDER*/}
        {animatedSVGContainer("red", 18, timer * 2 + 900, B, { zIndex: 2 })}
        {animatedSVGContainer("white", 20, timer * 3 + 400, B, { zIndex: 2 })}

        {/*LEFT BORDER*/}
        {animatedSVGContainer("orange", 18, timer * 3 + 900, L, { zIndex: 1 })}
        {animatedSVGContainer("white", 20, timer * 4 + 400, L, { zIndex: 1 })}

        {/*OPPOSITE BOTTOM BORDER*/}
        {animatedSVGContainer("brown", 18, 900, B)}
        {animatedSVGContainer("white", 20, timer + 400, B)}

        {/*OPPOSITE LEFT BORDER*/}
        {animatedSVGContainer("green", 18, timer + 900, L)}
        {animatedSVGContainer("white", 20, timer * 2 + 400, L)}

        {/*OPPOSITE TOP BORDER*/}
        {animatedSVGContainer("red", 18, timer * 2 + 900, T)}
        {animatedSVGContainer("white", 20, timer * 3 + 400, T)}

        {/*OPPOSITE RIGHT BORDER*/}
        {animatedSVGContainer("orange", 18, timer * 3 + 900, R)}
        {animatedSVGContainer("white", 20, timer * 4 + 400, R)}
      </View>
    );
  };

  return (
    <View>
      {totalDuration === 0 && renderSquareBorderAnimation()}
      {totalDuration === duration && renderSquareBorderAnimation()}
    </View>
  );
});

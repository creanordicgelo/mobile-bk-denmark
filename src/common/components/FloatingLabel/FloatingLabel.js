import React, { Component } from "react";
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  StyleSheet
} from "react-native";

import styles from "./styles";

import renderIf from "../../utils/renderIf";

class FloatingLabel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isFloatLabelVisible: false,
      value: this.props.value,
      placeholder: this.props.placeholder,
      textColor: this.props.textColor ? this.props.textColor : "white"
    };
  }

  // to show the floating label while typing
  showFloatingLabel = () => {
    this.setState({
      isFloatLabelVisible: true,
      placeholder: ""
    });
  };

  // to hide floating label if input text has value
  hideFloatingLabel = () => {
    let isVisible = false;
    let placeholder = this.props.placeholder;

    // has value hide floating text
    if (this.state.value.length > 0) {
      placeholder = "";
      isVisible = true;
    }

    this.setState({
      isFloatLabelVisible: isVisible,
      placeholder
    });
  };

  // to update the changes in the input and activate it
  onUpdateInputValue = text => {
    this.setState({
      value: text
    });
    this.props.onChangeText(text);
  };

  render() {
    let { isFloatLabelVisible } = this.state;

    return (
      <View style={styles.container}>
        <View>
          {renderIf(
            isFloatLabelVisible,
            <Text style={styles.floatingText}>{this.props.placeholder}</Text>
          )}
          <TextInput
            className="floating-label"
            type="text"
            placeholder={this.state.placeholder}
            placeholderTextColor="#f2f2f2"
            style={[styles.textInput, { color: this.state.textColor }]}
            selectionColor={this.state.textColor}
            value={this.state.value}
            onFocus={this.showFloatingLabel}
            onBlur={this.hideFloatingLabel}
            onChangeText={this.onUpdateInputValue}
            secureTextEntry={this.props.secureTextEntry}
          />
        </View>
      </View>
    );
  }
}

export default FloatingLabel;

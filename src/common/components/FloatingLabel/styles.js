import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    minHeight: 48,
    marginTop: 16
  },
  floatingText: { color: "white", fontWeight: "bold", fontSize: 12 },
  textInput: {
    fontFamily: "AvenirNext-Medium",
    paddingBottom: 8,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderWidth: 1,
    borderColor: "white",
    color: "white",
    fontSize: 18
  }
});

export default styles;

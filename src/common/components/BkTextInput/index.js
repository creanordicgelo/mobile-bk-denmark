import React from "react";
import PropTypes from "prop-types";
import { View, ImageBackground, TextInput, Platform } from "react-native";

const BkTextInput = ({
  onUpdateInputValue,
  textInput,
  placeHolder,
  secureTextEntry
}) => (
  <View style={{ position: "relative" }}>
    <ImageBackground
      style={{
        flex: 1,
        height: Platform.OS === "ios" ? 40 : 48
      }}
      source={require("../../../images/bg_input.png")}
    />
    <TextInput
      type="text"
      secureTextEntry={secureTextEntry}
      placeholder={placeHolder}
      placeholderStyle={{ fontWeight: "bold" }}
      placeholderTextColor="#ec7601"
      value={textInput}
      onChangeText={text => {
        onUpdateInputValue(text);
      }}
      style={{
        color: "white",
        fontSize: 12.5,
        top: Platform.OS === "ios" ? 0 : 4,
        fontWeight: "bold",
        position: "absolute",
        paddingLeft: 12,
        paddingRight: 12,
        width: "100%",
        height: 40
      }}
      multiline={false}
      autoCapitalize="none"
    />
  </View>
);

BkTextInput.propTypes = {
  onUpdateInputValue: PropTypes.func,
  textInput: PropTypes.string,
  placeHolder: PropTypes.string,
  secureTextEntry: PropTypes.bool
};

BkTextInput.defaultProps = {
  onChangeText: () => {},
  textInput: "",
  placeHolder: "",
  secureTextEntry: false
};

export default BkTextInput;

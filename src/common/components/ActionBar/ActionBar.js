import React from "react";
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  Platform,
  Linking
} from "react-native";
import { Actions } from "react-native-router-flux";
import * as Animatable from "react-native-animatable";
import { isUndefined } from "lodash";
import { isIphoneX } from "react-native-iphone-x-helper";

import styles from "./styles";
import renderIf from "../../utils/renderIf";
import textStyles from "../../values/textStyles";

import lang from "../../../config/strings.json";

export default class ActionBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowingMenu: false
    };
    this.viewMenu = [];
  }

  onCloseMenu = () => {
    this.setState({
      isShowingMenu: false
    });
  };

  render() {
    let { isShowingMenu } = this.state;

    let { showBack = false, onBackClick } = this.props;

    return (
      <View style={{ position: "absolute", width: "100%" }}>
        <View
          style={{
            padding: 24,
            paddingBottom: 10,
            marginTop:
              isIphoneX() === true ? 32 : Platform.OS === "ios" ? 16 : 0
          }}
        >
          {showBack && (
            <TouchableOpacity
              style={{ position: "absolute", left: 24, top: 32 }}
              onPress={() => {
                if (isUndefined(onBackClick)) {
                  Actions.pop();
                } else {
                  onBackClick();
                }
              }}
            >
              <Image
                style={styles.imgBack}
                source={require("../../../images/ic_back_yellow.png")}
              />
            </TouchableOpacity>
          )}

          <TouchableOpacity
            style={{ position: "absolute", right: 24, top: 20 }}
            onPress={() => {
              Actions.reset("home");
            }}
          >
            <Image
              style={styles.imgLogo}
              source={require("../../../images/ic_logo.png")}
            />
          </TouchableOpacity>
        </View>

        {this.state.isShowingMenu === true && (
          <View>
            <Animatable.View
              ref={ref => {
                this.viewMenu[4] = ref;
              }}
            >
              <View
                style={{
                  backgroundColor: "#62a323",
                  height: 50,
                  justifyContent: "center"
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    Actions.replace("home");
                    Actions.coupons();
                    this.onCloseMenu();
                  }}
                >
                  <View flexDirection="row">
                    <Text style={textStyles.txtMenuHeaderTitle}>
                      {lang.menu.coupons}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </Animatable.View>
            <Animatable.View
              ref={ref => {
                this.viewMenu[3] = ref;
              }}
            >
              <View
                style={{
                  backgroundColor: "#df2527",
                  height: 50,
                  justifyContent: "center"
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    Actions.replace("home");
                    Actions.campaign();
                    this.onCloseMenu();
                  }}
                >
                  <View flexDirection="row">
                    <Text style={textStyles.txtMenuHeaderTitle}>
                      {lang.menu.campaign}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </Animatable.View>
            <Animatable.View
              ref={ref => {
                this.viewMenu[2] = ref;
              }}
            >
              <View
                style={{
                  backgroundColor: "#622817",
                  height: 50,
                  justifyContent: "center"
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    Actions.replace("home");
                    Actions.restaurant();
                    this.onCloseMenu();
                  }}
                >
                  <View flexDirection="row">
                    <Text style={textStyles.txtMenuHeaderTitle}>
                      {lang.menu.restaurant}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </Animatable.View>
            <Animatable.View
              ref={ref => {
                this.viewMenu[1] = ref;
              }}
            >
              <View
                style={{
                  backgroundColor: "#f6b031",
                  height: 50,
                  justifyContent: "center"
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    Actions.replace("home");
                    Actions.menu();
                    this.onCloseMenu();
                  }}
                >
                  <View flexDirection="row">
                    <Text style={textStyles.txtMenuHeaderTitle}>
                      {lang.menu.bk_menu}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </Animatable.View>
            <Animatable.View
              ref={ref => {
                this.viewMenu[0] = ref;
              }}
            >
              <View
                style={{
                  backgroundColor: "#8b5332",
                  height: 50,
                  justifyContent: "center"
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    const url = "https://www.whopperlab.dk/bkd/bestill?2";
                    Linking.openURL(url).catch(err =>
                      console.error("An error occurred", err)
                    );
                  }}
                >
                  <View flexDirection="row">
                    <Text style={textStyles.txtMenuHeaderTitle}>
                      {lang.menu.delivery}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </Animatable.View>
          </View>
        )}
      </View>
    );
  }
}

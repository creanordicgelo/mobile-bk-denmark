import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const styles = StyleSheet.create({
  imgMenu: {
    width: 28,
    height: 28
  },
  imgLogo: {
    width: 40,
    height: 44
  },
  imgBack: {
    width: 24,
    height: 20
  },
  txtMenu: {
    fontSize: 16,
    color: "white"
  },
  viewHorizontalLine: {
    flex: 1,
    height: 1,
    borderBottomColor: "silver",
    borderBottomWidth: 0.5
  }
});

export default styles;

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  imgCrown: {
    height: 60,
    width: 88
  }
});

export default styles;

import React from "react";
import PropTypes from "prop-types";
import { View, Text, Image } from "react-native";

import constants from "../../values";
import textStyles from "../../values/textStyles";
import styles from "./styles";

const Crown = ({ crownType, title }) => (
  <View style={{ justifyContent: "center", alignItems: "center" }}>
    <Image
      style={styles.imgCrown}
      source={
        crownType === constants.crownType.yellow
          ? require("../../../images/img_crown.png")
          : require("../../../images/img_black_crown.png")
      }
    />

    <Text
      style={[
        textStyles.txtCalloutStyle,
        {
          color:
            crownType === constants.crownType.yellow ? "#f8b016" : "#404042",
          fontWeight: "bold",
          fontSize: 18,
          marginTop: 16
        }
      ]}
    >
      {title}
    </Text>
  </View>
);

Crown.propTypes = {
  crownType: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default Crown;

import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const profileMenuWidth = widthDimension - 80;
const profileMenuUnit = profileMenuWidth / 210;
const profileMenuHeight = profileMenuUnit * 47;

const styles = StyleSheet.create({
  imgMenu: {
    width: profileMenuWidth,
    height: profileMenuHeight
  }
});

export default styles;

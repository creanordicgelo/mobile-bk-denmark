import React from "react";
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  Platform,
  Linking
} from "react-native";
import { Actions } from "react-native-router-flux";
import * as Animatable from "react-native-animatable";
import * as _ from "lodash";
import { isIphoneX } from "react-native-iphone-x-helper";

import styles from "./styles";
import renderIf from "../../utils/renderIf";

export default class BKProfile extends React.Component {
  anonymousUserRender = () => {
    return (
      <TouchableOpacity
        style={styles.imgMenu}
        onPress={() => {
          this.props.onPress();
        }}
      >
        <Image
          style={styles.imgMenu}
          source={require("../../../images/bk_profile.png")}
        />
      </TouchableOpacity>
    );
  };

  render() {
    let { isLoggedIn } = this.props;

    return isLoggedIn === true ? null : this.anonymousUserRender();
  }
}

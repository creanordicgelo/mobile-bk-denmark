import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity } from "react-native";
import { Icon } from "native-base";
import { Dropdown } from "react-native-material-dropdown";

const BKDropDown = ({ dataParams, value, onChange, fontStyle }) => (
  <Dropdown
    data={dataParams}
    fontSize={12}
    containerStyle={{ flex: 1 }}
    rippleOpacity={0}
    itemCount={3}
    dropdownPosition={0}
    renderBase={props => {
      return (
        <View
          style={{
            backgroundColor: "#f8b016",
            padding: 8,
            height: 30,
            alignItems: "center",
            justifyContent: "space-between",
            flexDirection: "row"
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 10
            }}
          >
            {props.value}
          </Text>
          <Icon
            name="ios-arrow-down"
            style={{ marginLeft: 2, fontSize: 16, color: "white" }}
          />
        </View>
      );
    }}
    value={value}
    onChangeText={value => {
      onChange(value);
    }}
  />
);

BKDropDown.propTypes = {
  fontStyle: PropTypes.object,
  onChange: PropTypes.func,
  dataParams: PropTypes.array,
  value: PropTypes.string
};

BKDropDown.defaultProps = {
  fontStyle: {},
  dataParams: [],
  value: "",
  onChange: () => {}
};

export default BKDropDown;

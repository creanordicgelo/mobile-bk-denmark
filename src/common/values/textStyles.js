import { StyleSheet, Platform } from "react-native";

const textStyles = StyleSheet.create({
  txtMenuLarge: {
    fontFamily: "AvenirNext-HeavyItalic",
    color: "white",
    fontSize: 48
  },
  txtMenuTitle: {
    fontFamily:
      Platform.OS === "ios" ? "BlockPro-Condensed" : "BlockProPlusCon",
    color: "#606060",
    fontSize: 36
  },
  txtMenuHeaderTitle: {
    fontFamily:
      Platform.OS === "ios" ? "BlockPro-Condensed" : "BlockProPlusCon",
    color: "white",
    marginLeft: 24,
    fontSize: 24
  },
  txtSubMenuHeaderTitle: {
    fontFamily:
      Platform.OS === "ios" ? "BlockPro-Condensed" : "BlockProPlusCon",
    color: "white",
    marginLeft: 24,
    fontSize: 20
  },
  txtCalloutStyle: {
    fontFamily:
      Platform.OS === "ios" ? "BlockPro-Condensed" : "BlockProPlusCon",
    color: "black"
  },
  txtMenuHeaders: {
    fontFamily: "AvenirNext-HeavyItalic",
    color: "white",
    fontSize: 14
  },
  txtMenu: {
    fontFamily: "AvenirNext-HeavyItalic",
    color: "white",
    fontSize: 18
  },
  txtHeadline1: {
    fontFamily: "AvenirNext-HeavyItalic",
    color: "white",
    fontSize: 30
  },
  txtSmallText: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 14,
    color: "white"
  },
  txtMedium: {
    fontFamily: "AvenirNext-Medium",
    fontSize: 14,
    color: "white"
  },
  txtError: {
    fontFamily: "AvenirNext-Regular",
    fontSize: 14,
    color: "red"
  }
});

export default textStyles;

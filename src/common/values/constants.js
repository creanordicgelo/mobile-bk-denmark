//UI
export const SHOW_MENU = "SHOW_MENU";
export const HIDE_MENU = "HIDE_MENU";

//RESTAURANTS
export const GET_RESTAURANTS = "GET_RESTAURANTS";

// GDPR
export const AGREED_TO_GDPR = "AGREED_TO_GDPR";

// USER
export const USER_DATA = "USER_DATA";
export const USER_REGISTER = "USER_REGISTER";
export const USER_LOGIN = "USER_LOGIN";
export const USER_SET_LOGIN = "USER_SET_LOGIN";
export const USER_LOGOUT = "USER_LOGOUT";
export const USER_EDIT = "USER_EDIT";
export const USER_DELETE = "USER_DELETE";
export const USER_FB_SIGNINVALIDATE = "USER_FB_SIGNINVALIDATE";
export const UPDATE_LOGIN_TYPE = "UPDATE_LOGIN_TYPE";
export const UPDATE_LOGIN_ACTION_LOCATION = "UPDATE_LOGIN_ACTION_LOCATION";

// COUPONS
export const GET_COUPONS = "GET_COUPONS";
export const CLAIM_COUPON = "CLAIM_COUPON";
export const CLAIM_MEMBERS_COUPON = "CLAIM_MEMBERS_COUPON";
export const GET_COUPON_BY_ID = "GET_COUPON_BY_ID";
export const GET_CLAIMED_COUPONS = "GET_CLAIMED_COUPONS";
export const CLAIMED_COUPONS = "CLAIMED_COUPONS";

//CAMPAIGNS
export const GET_CAMPAIGNS = "GET_CAMPAIGNS";

// MENU
export const CLEAR_MENU = "CLEAR_MENU";
export const GET_CATEGORY = "GET_CATEGORY";
export const GET_MENU = "GET_MENU";
export const GET_MENU_BY_CATEGORY = "GET_MENU_BY_CATEGORY";

// LINKS
export const GET_LINKS = "GET_LINKS";

// SERVER SWITCH
export const IS_STAGING_ENVIRONMENT = "IS_STAGING_ENVIRONMENT";
export const SET_UP_SERVER = "SET_UP_SERVER";
export const PROD_URL = "https://burgerking-denmark.herokuapp.com";
export const STAGING_URL = "https://burgerking-denmark-staging.herokuapp.com";

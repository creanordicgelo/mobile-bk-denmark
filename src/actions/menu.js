import axios from "axios";

import { Actions } from "react-native-router-flux";

import {
  CLEAR_MENU,
  GET_MENU,
  GET_MENU_BY_CATEGORY,
  GET_CATEGORY
} from "../common/values/constants";

import { API_BASE_URL, AUTHORIZATION_KEY } from "../config/settings";
import { mobileBKApiRequest } from "../common/client/client";

export const getMenu = () => dispatch => {
  dispatch({
    type: GET_MENU,
    payload: axios({
      method: "GET",
      url: API_BASE_URL + `/menu`,
      headers: {
        "X-Authorization": AUTHORIZATION_KEY
      }
    })
  });
};

export const getMenuByCategory = category => dispatch => {
  dispatch({
    type: GET_MENU_BY_CATEGORY,
    payload: mobileBKApiRequest().get(
      `product/getallproduct?category=${category}`
    )
  });
};

export const clearMenu = () => dispatch => {
  dispatch({
    type: CLEAR_MENU
  });
};

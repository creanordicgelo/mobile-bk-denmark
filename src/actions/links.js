import { mobileBKApiRequest } from "../common/client/client";
import { GET_LINKS } from "../common/values/constants";
import { isUndefined } from "lodash";

export const getLinks = callback => dispatch => {
  dispatch({
    type: GET_LINKS,
    payload: mobileBKApiRequest().get("home/links")
  })
    .then(object => {
      if (object.action.type === GET_LINKS + "_FULFILLED") {
        if (!isUndefined(callback)) {
          callback();
        }
      }
    })
    .catch(err => {
      if (!isUndefined(callback)) {
        callback();
      }
    });
};

import axios from "axios";
import * as _ from "lodash";
import SInfo from "react-native-sensitive-info";
import { getLinks } from "./links";

import {
  IS_STAGING_ENVIRONMENT,
  SET_UP_SERVER,
  PROD_URL,
  STAGING_URL,
  AGREED_TO_GDPR,
  USER_DATA,
  CLAIMED_COUPONS
} from "../common/values/constants";

export const setServer = () => dispatch => {
  dispatch({
    type: SET_UP_SERVER,
    payload: SInfo.getItem(IS_STAGING_ENVIRONMENT, {}).then(value => {
      if (!value || _.isUndefined(value) || value == "false") {
        return PROD_URL;
      }
      return STAGING_URL;
    })
  });
};

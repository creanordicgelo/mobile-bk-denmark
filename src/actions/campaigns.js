import axios from "axios";
import * as _ from "lodash";

import { GET_CAMPAIGNS } from "../common/values/constants";

import { API_BASE_URL, AUTHORIZATION_KEY } from "../config/settings";
import { mobileBKApiRequest } from "../common/client/client";

export const getCampaigns = () => dispatch => {
  dispatch({
    type: GET_CAMPAIGNS,
    payload: mobileBKApiRequest().get("campaign/getallcampaign/active")
  });
};

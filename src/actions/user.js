import axios from "axios";
import firebase from "react-native-firebase";

import {
  USER_REGISTER,
  USER_LOGIN,
  USER_SET_LOGIN,
  USER_LOGOUT,
  USER_EDIT,
  USER_DELETE,
  USER_FB_SIGNINVALIDATE,
  UPDATE_LOGIN_TYPE,
  UPDATE_LOGIN_ACTION_LOCATION
} from "../common/values/constants";
import {
  mobileBKApiRequest,
  mobileBKAuthorizedApiRequest
} from "../common/client/client";
import { registerUserPushNotification } from "./pushNotif";

export const userRegister = (
  username,
  password,
  email,
  gender,
  birthyear,
  errorCallback
) => dispatch => {
  dispatch({
    type: USER_REGISTER,
    payload: mobileBKApiRequest().post("user/register", {
      username,
      password,
      email,
      gender,
      birthyear
    })
  })
    .then(object => {
      if (object.action.type === USER_REGISTER + "_FULFILLED") {
        dispatch(userLogin(email, password, "register"));
      }
    })
    .catch(error => {
      if (error) {
        errorCallback(error.response.data.Message);
      }
    });
};

export const userEdit = (
  token,
  username,
  password,
  gender,
  birthyear,
  errorCallback
) => dispatch => {
  dispatch({
    type: USER_EDIT,
    payload: mobileBKAuthorizedApiRequest(token).put("user/edituser", {
      username,
      password,
      gender,
      birthyear
    })
  }).catch(error => {
    if (error) {
      errorCallback(error.response.data.Message);
    }
  });
};

export const setLoggedIn = userData => dispatch => {
  dispatch({
    type: USER_SET_LOGIN,
    payload: userData
  });
};

export const userLogin = (
  email,
  password,
  loginType,
  errorCallback
) => dispatch => {
  dispatch(updateLoginType(loginType));

  dispatch({
    type: USER_LOGIN,
    payload: mobileBKApiRequest().post("user/login", {
      email,
      password
    })
  })
    .then(response => {
      if (response.action.type === USER_LOGIN + "_FULFILLED") {
        const { token } = response.action.payload.data;
        firebase
          .messaging()
          .getToken()
          .then(fcmToken => {
            if (fcmToken) {
              // user has a device token
              registerUserPushNotification(fcmToken, token);
            } else {
              // user doesn't have a device token yet
            }
          });
      }
    })
    .catch(error => {
      if (error) {
        errorCallback(error.response.data.Message);
      }
    });
};

export const updateLoginLocation = loginLocation => dispatch => {
  dispatch({
    type: UPDATE_LOGIN_ACTION_LOCATION,
    payload: {
      loginLocation
    }
  });
};

export const updateLoginType = loginType => dispatch => {
  dispatch({
    type: UPDATE_LOGIN_TYPE,
    payload: {
      loginType
    }
  });
};

export const userLogout = token => dispatch => {
  dispatch({
    type: USER_LOGOUT,
    payload: mobileBKAuthorizedApiRequest(token).post("user/signout")
  });
};

export const forgotPassword = (email, callback) => {
  mobileBKApiRequest()
    .post("user/forgotpassword", { email })
    .then(response => {
      callback(true);
    })
    .catch(err => {
      callback(false);
    });
};

export const userDelete = token => dispatch => {
  dispatch({
    type: USER_DELETE,
    payload: mobileBKAuthorizedApiRequest(token).delete("user/deleteaccount")
  });
};

export const loginCreateViaFB = (accesstoken, facebookid) => dispatch => {
  dispatch(updateLoginType("fb"));

  dispatch({
    type: USER_FB_SIGNINVALIDATE,
    payload: mobileBKApiRequest().post("user/auth/facebook/validate", {
      accesstoken,
      facebookid
    })
  }).then(response => {
    if (response.action.type === USER_FB_SIGNINVALIDATE + "_FULFILLED") {
      const { token } = response.action.payload.data;
      firebase
        .messaging()
        .getToken()
        .then(fcmToken => {
          if (fcmToken) {
            // user has a device token
            registerUserPushNotification(fcmToken, token);
          } else {
            // user doesn't have a device token yet
          }
        });
    }
  });
};

import axios from "axios";

import { GET_RESTAURANTS } from "../common/values/constants";
import { API_BASE_URL, AUTHORIZATION_KEY } from "../config/settings";
import { mobileBKApiRequest } from "../common/client/client";

export const getRestaurants = () => {
  return {
    type: GET_RESTAURANTS,
    payload: mobileBKApiRequest().get("restaurant/getallrestaurant")
  };
};

export const getDeliveryUrl = callback => {
  mobileBKApiRequest()
    .get("delivery/getlink")
    .then(response => {
      let url = response.data.link;
      callback(url);
    });
};

import axios from "axios";
import DeviceInfo from "react-native-device-info";
import SInfo from "react-native-sensitive-info";

import { mobileBKApiRequest } from "../common/client/client";
import { AGREED_TO_GDPR } from "../common/values/constants";

export const getGDPRMessage = callback => {
  mobileBKApiRequest()
    .get("gdpr/mobile/fetchgdpr")
    .then(response => {
      callback(response.data.Message);
    });
};

export const acceptGDPR = callback => {
  mobileBKApiRequest()
    .post("gdpr/mobile/acceptgdpr", {
      deviceid: DeviceInfo.getUniqueID()
    })
    .then(response => {
      SInfo.setItem(AGREED_TO_GDPR, "true", {}).then(response => {
        callback();
      });
    });
};

import DeviceInfo from "react-native-device-info";
import SInfo from "react-native-sensitive-info";
import axios from "axios";
import * as _ from "lodash";
import moment from "moment";

import {
  GET_COUPONS,
  CLAIM_COUPON,
  GET_COUPON_BY_ID,
  CLAIMED_COUPONS,
  GET_CLAIMED_COUPONS,
  CLAIM_MEMBERS_COUPON
} from "../common/values/constants";

import {
  mobileBKApiRequest,
  mobileBKAuthorizedApiRequest
} from "../common/client/client";

export const getCoupons = () => dispatch => {
  dispatch({
    type: GET_COUPONS,
    payload: mobileBKApiRequest().get(
      `coupon/mobile/fetchactive?deviceid=${DeviceInfo.getUniqueID()}`
    )
  });
};

export const getCouponsLoggedIn = token => dispatch => {
  dispatch({
    type: GET_COUPONS,
    payload: mobileBKAuthorizedApiRequest(token).get(
      `coupon/mobile/fetchactive?deviceid=${DeviceInfo.getUniqueID()}`
    )
  });
};

export const getClaimedCoupons = () => dispatch => {
  dispatch({
    type: GET_CLAIMED_COUPONS,
    payload: SInfo.getItem(CLAIMED_COUPONS, {})
  });
};

export const claimNonMembersCoupon = (couponId, callback) => dispatch => {
  dispatch({
    type: CLAIM_COUPON,
    payload: mobileBKApiRequest().get(
      `coupon/claim/nonmember?couponid=${couponId}&deviceid=${DeviceInfo.getUniqueID()}`
    )
  })
    .then(response => {
      if (response.action.type === CLAIM_COUPON + "_FULFILLED") {
        let expiryTime = moment()
          .add(5, "minutes")
          .unix();

        SInfo.getItem(CLAIMED_COUPONS, {}).then(value => {
          let claimedCoupons = {};
          if (!_.isEmpty(value) && !_.isUndefined(value) && !_.isNull(value)) {
            claimedCoupons = JSON.parse(value);
          }

          claimedCoupons[couponId] = expiryTime;

          const stringData = JSON.stringify(claimedCoupons);
          SInfo.setItem(CLAIMED_COUPONS, stringData, {}).then(response => {
            dispatch(getClaimedCoupons());
          });
        });

        callback();
      }
    })
    .catch(err => console.log(err.response));
};

export const claimMembersCoupon = (couponId, token, callback) => dispatch => {
  dispatch({
    type: CLAIM_MEMBERS_COUPON,
    payload: mobileBKAuthorizedApiRequest(token).get(
      `coupon/claim/member?couponid=${couponId}`
    )
  }).then(response => {
    if (response.action.type === CLAIM_MEMBERS_COUPON + "_FULFILLED") {
      let expiryTime = moment()
        .add(5, "minutes")
        .unix();

      SInfo.getItem(CLAIMED_COUPONS, {}).then(value => {
        let claimedCoupons = {};
        if (!_.isEmpty(value) && !_.isUndefined(value) && !_.isNull(value)) {
          claimedCoupons = JSON.parse(value);
        }

        claimedCoupons[couponId] = expiryTime;

        const stringData = JSON.stringify(claimedCoupons);
        SInfo.setItem(CLAIMED_COUPONS, stringData, {}).then(response => {
          dispatch(getClaimedCoupons());
        });
      });

      callback();
    }
  });
};

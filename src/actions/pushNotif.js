import axios from "axios";
import {
  mobileBKApiRequest,
  mobileBKAuthorizedApiRequest
} from "../common/client/client";

export const registerPushNotification = token => {
  mobileBKApiRequest().post("registrationtoken/store", {
    registrationtoken: token
  });
};

export const registerUserPushNotification = (token, userToken) => {
  mobileBKAuthorizedApiRequest(userToken).post("registrationtoken/store", {
    registrationtoken: token
  });
};

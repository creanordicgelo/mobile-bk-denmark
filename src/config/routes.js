import React from "react";
import { connect } from "react-redux";
import { Scene, Actions, Router } from "react-native-router-flux";

import Campaign from "../screens/Campaign/Campaign";
import CouponDetail from "../screens/CouponDetail/CouponDetail";
import Coupons from "../screens/Coupons/Coupons";
import CreatedAccount from "../screens/Account/created-account";
import CreateProfile from "../screens/Account/create-profile";
import DeleteAccount from "../screens/Account/delete-account";
import EditProfile from "../screens/Account/edit-profile";
import Delivery from "../screens/Delivery/Delivery";
import Feedback from "../screens/Feedback/Feedback";
import ForgotPassword from "../screens/Account/forgot-password";
import Gdpr from "../screens/Gdpr";
import Home from "../screens/Home/Home";
import SplashScreen from "../screens/SplashScreen";
import LoginAccount from "../screens/Account/login-account";
import Menu from "../screens/Menu/Menu";
import MenuSelection from "../screens/MenuSelection/MenuSelection";
import ProfileInfo from "../screens/Account/profile-info";
import Restaurant from "../screens/Restaurant/Restaurant";
import Signup from "../screens/Signup/Signup";

export const AppNavigator = Actions.create(
  <Scene key="root" hideNavBar panHandlers={null}>
    <Scene key="campaign" component={Campaign} />
    <Scene key="couponDetail" component={CouponDetail} />
    <Scene key="coupons" component={Coupons} />
    <Scene key="createdAccount" component={CreatedAccount} />
    <Scene key="createProfile" component={CreateProfile} />
    <Scene key="deleteAccount" component={DeleteAccount} />
    <Scene key="editProfile" component={EditProfile} />
    <Scene key="delivery" component={Delivery} />
    <Scene key="feedback" component={Feedback} />
    <Scene key="forgotPassword" component={ForgotPassword} />
    <Scene key="home" component={Home} />
    <Scene key="login" component={LoginAccount} />
    <Scene key="gdpr" component={Gdpr} />
    <Scene key="menu" component={Menu} />
    <Scene key="menuSelection" component={MenuSelection} />
    <Scene key="profileInfo" component={ProfileInfo} />
    <Scene key="restaurant" component={Restaurant} />
    <Scene key="signup" component={Signup} />
    <Scene key="splash" component={SplashScreen} />
  </Scene>
);

const mapStateToProps = state => ({
  state: state.nav
});

export const ReduxRouter = connect(mapStateToProps)(Router);

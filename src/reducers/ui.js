import {
  GET_RESTAURANTS,
  GET_MENU,
  GET_MENU_BY_CATEGORY,
  GET_CAMPAIGNS,
  GET_COUPONS,
  CLAIM_COUPON,
  CLAIM_MEMBERS_COUPON,
  USER_LOGIN,
  USER_REGISTER,
  USER_EDIT,
  USER_FB_SIGNINVALIDATE
} from "../common/values/constants";

const INITIAL_STATE = {
  isLoading: false
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case GET_RESTAURANTS + "_FULFILLED":
    case GET_MENU + "_FULFILLED":
    case GET_MENU_BY_CATEGORY + "_FULFILLED":
    case GET_CAMPAIGNS + "_FULFILLED":
    case GET_COUPONS + "_FULFILLED":
    case CLAIM_COUPON + "_FULFILLED":
    case CLAIM_MEMBERS_COUPON + "_FULFILLED":
    case USER_LOGIN + "_FULFILLED":
    case USER_REGISTER + "_FULFILLED":
    case USER_EDIT + "_FULFILLED":
    case USER_FB_SIGNINVALIDATE + "_FULFILLED":
      return {
        ...state,
        isLoading: false
      };
    case GET_RESTAURANTS + "_REJECTED":
    case GET_MENU + "_REJECTED":
    case GET_MENU_BY_CATEGORY + "_REJECTED":
    case GET_CAMPAIGNS + "_REJECTED":
    case GET_COUPONS + "_REJECTED":
    case CLAIM_COUPON + "_REJECTED":
    case CLAIM_MEMBERS_COUPON + "_REJECTED":
    case USER_LOGIN + "_REJECTED":
    case USER_LOGIN + "_REJECTED":
    case USER_EDIT + "_REJECTED":
    case USER_FB_SIGNINVALIDATE + "_REJECTED":
      return {
        ...state,
        isLoading: false
      };
    case GET_RESTAURANTS + "_PENDING":
    case GET_MENU + "_PENDING":
    case GET_MENU_BY_CATEGORY + "_PENDING":
    case GET_CAMPAIGNS + "_PENDING":
    case GET_COUPONS + "_PENDING":
    case CLAIM_COUPON + "_PENDING":
    case CLAIM_MEMBERS_COUPON + "_PENDING":
    case USER_LOGIN + "_PENDING":
    case USER_LOGIN + "_PENDING":
    case USER_EDIT + "_PENDING":
    case USER_FB_SIGNINVALIDATE + "_PENDING":
      return {
        ...state,
        isLoading: true
      };
    default:
      return state;
  }
};

import {
  USER_LOGIN,
  USER_REGISTER,
  USER_DATA,
  USER_SET_LOGIN,
  USER_LOGOUT,
  USER_EDIT,
  USER_DELETE,
  USER_FB_SIGNINVALIDATE,
  UPDATE_LOGIN_ACTION_LOCATION,
  UPDATE_LOGIN_TYPE
} from "../common/values/constants";
import SInfo from "react-native-sensitive-info";
import * as _ from "lodash";
import { Actions } from "react-native-router-flux";

const INITIAL_STATE = {
  error: null,
  isLoggedIn: false,
  loginActionLocation: null,
  loginType: null,
  user: {}
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case USER_REGISTER + "_PENDING": {
      return {
        ...state,
        error: null
      };
    }
    case USER_REGISTER + "_FULFILLED": {
      return {
        ...state
      };
    }

    case USER_REGISTER + "_REJECTED": {
      return {
        ...state,
        error: true,
        loginType: null,
        isLoggedIn: false
      };
    }

    case USER_LOGIN + "_PENDING": {
      return {
        ...state,
        error: null
      };
    }

    case UPDATE_LOGIN_TYPE: {
      const { loginType } = action.payload;

      return {
        ...state,
        loginType
      };
    }

    case USER_LOGIN + "_FULFILLED": {
      const { profile, token } = action.payload.data;

      let user = {
        profile,
        token
      };

      SInfo.setItem(USER_DATA, JSON.stringify(user), {});

      return {
        ...state,
        error: null,
        isLoggedIn: true,
        user
      };
    }

    case USER_LOGIN + "_REJECTED": {
      return {
        ...state,
        error: true,
        loginType: null,
        isLoggedIn: false
      };
    }

    case USER_SET_LOGIN: {
      return {
        ...state,
        isLoggedIn: true,
        error: false,
        user: action.payload
      };
    }

    case USER_LOGOUT + "_FULFILLED": {
      SInfo.setItem(USER_DATA, "", {});

      return {
        ...state,
        error: false,
        user: {},
        loginType: null,
        isLoggedIn: false
      };
    }

    case USER_EDIT + "_PENDING": {
      return {
        ...state,
        error: null
      };
    }
    case USER_EDIT + "_FULFILLED": {
      let user = {
        profile: action.payload.data.data,
        token: state.user.token
      };

      SInfo.setItem(USER_DATA, JSON.stringify(user), {});

      return {
        ...state,
        error: null,
        user
      };
    }

    case USER_EDIT + "_REJECTED": {
      return {
        ...state,
        error: true
      };
    }

    case USER_DELETE + "_FULFILLED": {
      SInfo.setItem(USER_DATA, "", {});

      return {
        ...state,
        error: null,
        user: {},
        loginType: null,
        isLoggedIn: false
      };
    }

    case USER_FB_SIGNINVALIDATE + "_PENDING": {
      return {
        ...state,
        error: null
      };
    }

    case USER_FB_SIGNINVALIDATE + "_FULFILLED": {
      let user = {
        profile: action.payload.data.profile,
        token: action.payload.data.token
      };

      SInfo.setItem(USER_DATA, JSON.stringify(user), {});

      return {
        ...state,
        error: null,
        isLoggedIn: true,
        user
      };
    }

    case UPDATE_LOGIN_ACTION_LOCATION: {
      const { loginLocation } = action.payload;

      return {
        ...state,
        loginLocation
      };
    }

    default:
      return state;
  }
};

import { ActionConst } from "react-native-router-flux";

const INITIAL_STATE = { data: [], currentScene: "home" };

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case ActionConst.FOCUS:
      return {
        ...state,
        currentScene: action.routeName,
        data: action.params.data
      };
    default:
      return state;
  }
};

import { GET_LINKS } from "../common/values/constants";

const INITIAL_STATE = {
  links: {}
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case GET_LINKS + "_FULFILLED": {
      let newData = action.payload.data;

      return {
        links: newData
      };
    }

    default:
      return state;
  }
};

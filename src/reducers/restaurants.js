import { GET_RESTAURANTS } from "../common/values/constants";

const INITIAL_STATE = {
  locations: []
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case GET_RESTAURANTS + "_FULFILLED":
      const restoLocations = action.payload.data.data;

      const modifiedLocations = restoLocations.map(restaurantLocation => {
        let modifiedRestoLocation = restaurantLocation;

        const openingHourArray = modifiedRestoLocation.OpeningHours.split(",");

        const modifiedOpeningHourArray = openingHourArray.map(opening => {
          let modifiedOpening = {};

          if (opening.includes("Man: Restaurant ")) {
            modifiedOpening.day = "MANDAG";
            modifiedOpening.time = opening.replace("Man: Restaurant ", "");
          } else if (opening.includes("Tirs: Restaurant ")) {
            modifiedOpening.day = "TIRSDAG";
            modifiedOpening.time = opening.replace("Tirs: Restaurant ", "");
          } else if (opening.includes("Ons: Restaurant ")) {
            modifiedOpening.day = "ONSDAG";
            modifiedOpening.time = opening.replace("Ons: Restaurant ", "");
          } else if (opening.includes("Tors: Restaurant ")) {
            modifiedOpening.day = "TORSDAG";
            modifiedOpening.time = opening.replace("Tors: Restaurant ", "");
          } else if (opening.includes("Fre: Restaurant ")) {
            modifiedOpening.day = "FREDAG";
            modifiedOpening.time = opening.replace("Fre: Restaurant ", "");
          } else if (opening.includes("Lør: Restaurant ")) {
            modifiedOpening.day = "LØRDAG";
            modifiedOpening.time = opening.replace("Lør: Restaurant ", "");
          } else if (opening.includes("Søn: Restaurant ")) {
            modifiedOpening.day = "SØNDAG";
            modifiedOpening.time = opening.replace("Søn: Restaurant ", "");
          }

          return modifiedOpening;
        });

        modifiedRestoLocation.openingHourArray = modifiedOpeningHourArray;

        return modifiedRestoLocation;
      });

      return {
        ...state,
        locations: modifiedLocations
      };
    default:
      return state;
  }
};

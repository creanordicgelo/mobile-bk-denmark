import { combineReducers } from "redux";

import coupons from "./coupons";
import campaigns from "./campaigns";
import restaurants from "./restaurants";
import menu from "./menu";
import user from "./user";
import links from "./links";
import ui from "./ui";
import config from "./config";

import reducer from "./reducer";

import { AppNavigator } from "../config/routes";

const initialState = AppNavigator.router.getStateForAction(
  AppNavigator.router.getActionForPathAndParams("splash")
);

const navReducer = (state = initialState, action) => {
  const nextState = AppNavigator.router.getStateForAction(action, state);
  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
};

const AppReducer = combineReducers({
  nav: navReducer,
  coupons,
  campaigns,
  menu,
  reducer,
  restaurants,
  user,
  links,
  ui,
  config
});

export default AppReducer;

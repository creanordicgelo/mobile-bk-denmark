import { GET_LINKS, PROD_URL, SET_UP_SERVER } from "../common/values/constants";

const INITIAL_STATE = {
  baseURL: PROD_URL
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case SET_UP_SERVER + "_FULFILLED": {
      let serverData = action.payload;

      return {
        ...state,
        baseURL: serverData
      };
    }

    default:
      return state;
  }
};

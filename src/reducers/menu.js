import * as _ from "lodash";
import {
  CLEAR_MENU,
  GET_MENU,
  GET_MENU_BY_CATEGORY,
  GET_CATEGORY
} from "../common/values/constants";

const INITIAL_STATE = {
  menu: null,
  products: [],
  category: ""
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case GET_MENU + "_FULFILLED": {
      let menu = action.payload.data;

      return {
        ...state,
        menu
      };
    }

    case GET_MENU_BY_CATEGORY + "_PENDING": {
      return {
        ...state
      };
    }

    case GET_MENU_BY_CATEGORY + "_FULFILLED": {
      let menuProducts = action.payload.data.data;

      let products = _.isUndefined(menuProducts)
        ? []
        : menuProducts.map(product => {
            let productModified = product;
            productModified.isShowMenu = false;
            productModified.ContainsModified = _.map(
              product.Contains,
              (value, key) => {
                return {
                  Title: key,
                  Obj: value
                };
              }
            );

            return productModified;
          });

      return {
        ...state,
        products
      };
    }

    case CLEAR_MENU: {
      return {
        ...state,
        category: "",
        products: []
      };
    }

    default:
      return state;
  }
};

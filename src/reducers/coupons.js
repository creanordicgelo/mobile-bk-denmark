import SInfo from "react-native-sensitive-info";
import * as _ from "lodash";
import { Actions } from "react-native-router-flux";

import {
  GET_COUPONS,
  CLAIM_COUPON,
  CLAIM_MEMBERS_COUPON,
  GET_COUPON_BY_ID,
  GET_CLAIMED_COUPONS
} from "../common/values/constants";

const INITIAL_STATE = {
  data: null,
  showLoading: false,
  claimedCoupons: {},
  errorMessage: ""
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case GET_COUPONS + "_PENDING": {
      return {
        ...state,
        data: null,
        showLoading: true
      };
    }

    case GET_COUPONS + "_ERROR": {
      return {
        ...state,
        errorMessage: errMsg,
        showLoading: false
      };
    }

    case GET_COUPONS + "_FULFILLED": {
      let newData = action.payload.data;

      return {
        ...state,
        errorMessage: "",
        showLoading: false,
        data: newData
      };
    }
    case GET_COUPON_BY_ID + "_PENDING": {
      return {
        ...state,
        data: null,
        showLoading: true
      };
    }

    case GET_COUPON_BY_ID + "_ERROR": {
      return {
        ...state,
        errorMessage: errMsg,
        showLoading: false
      };
    }

    case GET_COUPON_BY_ID + "_FULFILLED": {
      let newData = action.payload.data;

      return {
        ...state,
        errorMessage: "",
        showLoading: false
      };
    }

    case GET_CLAIMED_COUPONS + "_FULFILLED": {
      let claimedCoupons = {};
      if (!_.isUndefined(action.payload)) {
        claimedCoupons = JSON.parse(action.payload);
      }

      return {
        ...state,
        claimedCoupons
      };
    }

    default:
      return state;
  }
};

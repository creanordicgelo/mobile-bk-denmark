import SInfo from "react-native-sensitive-info";
import { Actions } from "react-native-router-flux";

import { GET_CAMPAIGNS } from "../common/values/constants";

const INITIAL_STATE = {
  data: null,
  showLoading: false,
  errorMessage: ""
};

export default (state = INITIAL_STATE, action = {}) => {
  switch (action.type) {
    case GET_CAMPAIGNS + "_PENDING": {
      return {
        ...state,
        data: null,
        showLoading: true
      };
    }

    case GET_CAMPAIGNS + "_ERROR": {
      return {
        ...state,
        errorMessage: errMsg,
        showLoading: false
      };
    }

    case GET_CAMPAIGNS + "_FULFILLED": {
      let newData = action.payload.data;
      console.log("CAMPAINGS", newData);

      return {
        errorMessage: "",
        showLoading: false,
        data: newData.data
      };
    }

    default:
      return state;
  }
};

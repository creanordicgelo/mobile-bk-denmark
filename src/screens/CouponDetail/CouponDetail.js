import React from "react";
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  Animated
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { Text } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";
import Modal from "react-native-modal";
import HTML from "react-native-render-html";
import DialogBox from "react-native-dialogbox";
import moment from "moment";
import * as _ from "lodash";
import AutoHeightImage from "react-native-auto-height-image";
import * as Animatable from "react-native-animatable";
import QRCode from "react-native-qrcode-svg";

import styles from "./styles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import Loading from "../../common/components/Loading/Loading";
import SquareTimer from "../../common/components/SquareTimer";
import {
  claimNonMembersCoupon,
  claimMembersCoupon,
  getCoupons,
  getCouponsLoggedIn
} from "../../actions/coupons";
import textStyles from "../../common/values/textStyles";

import lang from "../../config/strings.json";

const widthDimension = Dimensions.get("window").width;

class CouponDetail extends React.Component {
  constructor(props) {
    super(props);

    const { couponDetail, claimedCoupons } = props;

    const isClaimed = couponDetail.isOneTimeUse && couponDetail.isClaimed;
    const expiringTime =
      _.isUndefined(claimedCoupons[couponDetail._id]) === true
        ? 0
        : claimedCoupons[couponDetail._id];

    let isExpired = false;
    let remainingTime = 0;

    if (expiringTime) {
      const currentTime = moment().unix();

      // make expired if expiring time is less than current time
      isExpired = isClaimed && expiringTime < currentTime;

      if (remainingTime < 0 || _.isUndefined(remainingTime)) {
        remainingTime: 0;
      } else {
        // get remaing time
        remainingTime = expiringTime - currentTime;
      }
    }

    this.state = {
      showModal: false,
      isClaimed,
      isExpired,
      remainingTime
    };
  }

  componentDidMount() {
    const { isClaimed, isExpired } = this.state;

    if (isClaimed && !isExpired) {
      this.startRedeemCouponTimer();
    }
  }

  startRedeemCouponTimer = () => {
    this.interval = setInterval(() => {
      const currentTime = moment().unix();

      const { claimedCoupons, couponDetail } = this.props;
      const expiringTime =
        _.isUndefined(claimedCoupons[couponDetail._id]) === true
          ? 0
          : claimedCoupons[couponDetail._id];

      let remainingTime = expiringTime - currentTime;

      if (remainingTime < 0 || _.isUndefined(remainingTime)) {
        remainingTime = 0;

        clearInterval(this.interval);

        this.setState({ isExpired: true, isClaimed: true, remainingTime });
        return;
      }

      this.setState({ remainingTime });
    }, 1000);
  };

  onBackClicked = () => {
    Actions.pop();
  };

  onShowRedeemCoupon = () => {
    this.setState({ showModal: true });
  };

  onRedeemCoupons = () => {
    const { isLoggedIn, user, couponDetail } = this.props;

    if (isLoggedIn === true) {
      this.props.dispatch(getCouponsLoggedIn(user.token));
    } else {
      this.props.dispatch(getCoupons());
    }

    if (couponDetail.isOneTimeUse) {
      this.setState({ remainingTime: 300, isClaimed: true, isExpired: false });
      this.startRedeemCouponTimer();
    } else {
      this.setState({ isClaimed: true });
    }
  };

  convertHtml = message => {
    return `<div style="color:black"}">` + message + `</div>`;
  };

  render() {
    const { couponDetail, claimedCoupons, user } = this.props;
    const { isExpired, isClaimed, remainingTime } = this.state;

    let showQR =
      !couponDetail.isOneTimeUse ||
      (!this.state.isExpired && couponDetail.isOneTimeUse);

    let minutes = "--";
    let seconds = "--";

    if (remainingTime) {
      minutes = Math.floor(remainingTime / 60);
      seconds = remainingTime - minutes * 60;

      if (seconds < 10) {
        seconds = "0" + seconds;
      }

      if (minutes < 10) {
        minutes = "0" + minutes;
      }
    }

    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() === true ? 40 : 20
        }}
        source={require("../../images/bg.png")}
      >
        <ScrollView
          ref={ref => {
            this.scrollView = ref;
          }}
          style={{ paddingTop: 20 }}
        >
          <View style={styles.container}>
            <Animatable.View
              style={{ alignItems: "center", justifyContent: "center" }}
              ref={ref => {
                this.viewAnimate = ref;
              }}
            >
              <AutoHeightImage
                width={widthDimension * 0.9}
                source={{
                  uri: couponDetail.Image
                }}
              />

              {isClaimed && (
                <View>
                  <Text
                    style={[
                      textStyles.txtCalloutStyle,
                      {
                        textAlign: "center",
                        color: "black",
                        fontSize: 25,
                        marginBottom: 0
                      }
                    ]}
                  >
                    {couponDetail.AppCode}
                  </Text>
                  {showQR && (
                    <Animatable.View
                      style={{
                        marginTop: 30,
                        marginBottom: 10
                      }}
                      animation="bounceIn"
                      delay={900}
                      ref={ref => {
                        this.viewQR = ref;
                      }}
                    >
                      <SquareTimer duration={4000} />
                      <QRCode size={200} value={couponDetail.QRValue} />
                    </Animatable.View>
                  )}
                </View>
              )}
            </Animatable.View>

            {!isClaimed && (
              <TouchableOpacity
                style={{ marginTop: 24 }}
                onPress={() => {
                  this.viewAnimate.flipOutY(800).then(endState => {
                    this.viewAnimate.flipInY(800).then(endState => {});
                  });

                  this.setState({
                    showModal: true
                  });
                  return;
                }}
              >
                <Image
                  style={[styles.imgMenu]}
                  source={require("../../images/img_redeem.png")}
                />
              </TouchableOpacity>
            )}

            {isClaimed && !couponDetail.isOneTimeUse && (
              <Animatable.View
                ref={ref => (this.viewActive = ref)}
                animation="tada"
                easing="ease-out"
                iterationDelay={1000}
                delay={2000}
                iterationCount="infinite"
              >
                <ImageBackground
                  style={[
                    styles.imgMenu,
                    {
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: 24
                    }
                  ]}
                  source={require("../../images/img_aktiv.png")}
                >
                  <Text
                    style={[
                      textStyles.txtCalloutStyle,
                      { color: "white", fontSize: 36, marginBottom: 10 }
                    ]}
                  >
                    {"Aktiv"}
                  </Text>
                </ImageBackground>
              </Animatable.View>
            )}

            {isClaimed &&
              (isExpired || remainingTime <= 0) &&
              couponDetail.isOneTimeUse == true && (
                <ImageBackground
                  style={[
                    styles.imgMenu,
                    {
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: 24
                    }
                  ]}
                  source={require("../../images/img_expired.png")}
                />
              )}

            {isClaimed &&
              !isExpired &&
              remainingTime > 0 &&
              couponDetail.isOneTimeUse === true && (
                <ImageBackground
                  style={[
                    styles.imgMenu,
                    {
                      alignItems: "center",
                      justifyContent: "center",
                      marginTop: 24
                    }
                  ]}
                  source={require("../../images/img_timer.png")}
                >
                  <Text
                    style={[
                      textStyles.txtCalloutStyle,
                      { color: "white", fontSize: 36, marginBottom: 10 }
                    ]}
                  >
                    {`${minutes}:${seconds}`}
                  </Text>
                </ImageBackground>
              )}

            <View style={{ marginTop: 8 }}>
              <HTML html={this.convertHtml(couponDetail.Text)} />
            </View>
            <Image
              style={[styles.imgSlogan, { marginTop: 48 }]}
              source={require("../../images/img_slogan.png")}
            />
          </View>
        </ScrollView>

        <ActionBar showBack={true} onBackClicked={this.onBackClicked} />

        <Modal
          isVisible={this.state.showModal}
          style={{ alignItems: "center", justifyContent: "center" }}
          animationIn={"fadeIn"}
          animationOut={"fadeOut"}
        >
          <View
            style={{
              padding: 16,
              width: "90%",
              backgroundColor: "white",
              borderRadius: 10,
              alignItems: "center"
            }}
          >
            <Text
              style={[
                textStyles.txtCalloutStyle,
                { color: "red", fontSize: 24, marginBottom: 10 }
              ]}
            >
              {lang.coupon_details.note}
            </Text>

            <Text
              style={[
                textStyles.txtMedium,
                { textAlign: "center", color: "black", marginBottom: 10 }
              ]}
            >
              {!couponDetail.isOneTimeUse
                ? lang.coupon_details.activate_coupon_question_no_timer
                : lang.coupon_details.activate_coupon_question}
            </Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                height: 40
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  this.setState({ showModal: false });
                }}
                style={{
                  flex: 1,
                  backgroundColor: "red",
                  marginRight: 5,
                  borderRadius: 5,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text style={[textStyles.txtCalloutStyle, { color: "white" }]}>
                  {lang.coupon_details.disconnect}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ showModal: false, isClaimed: true });

                  if (couponDetail.MembersOnly == true) {
                    this.props.dispatch(
                      claimMembersCoupon(
                        couponDetail._id,
                        user.token,
                        this.onRedeemCoupons
                      )
                    );
                  } else {
                    this.props.dispatch(
                      claimNonMembersCoupon(
                        couponDetail._id,
                        this.onRedeemCoupons
                      )
                    );
                  }
                }}
                style={{
                  flex: 1,
                  backgroundColor: "green",
                  marginLeft: 5,
                  borderRadius: 5,
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                <Text style={[textStyles.txtCalloutStyle, { color: "white" }]}>
                  {lang.coupon_details.yes_please}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <DialogBox
          ref={ref => {
            this.dialogbox = ref;
          }}
        />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    claimedCoupons: state.coupons.claimedCoupons,
    isLoggedIn: state.user.isLoggedIn,
    user: state.user.user
  };
};

export default connect(mapStateToProps)(CouponDetail);

import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 32,
    justifyContent: "center",
    alignItems: "center"
  },
  imgMenu: {
    width: widthDimension * 0.9,
    height: (widthDimension * 0.7) / 3
  },
  imgFullWidth: {
    width: widthDimension * 0.95,
    height: widthDimension * 0.9
  },
  imgSlogan: {
    justifyContent: "center",
    alignSelf: "center",
    width: widthDimension * 0.55,
    height: (widthDimension * 0.55) / 4
    // position: "absolute",// bottom: 64
  }
});

export default styles;

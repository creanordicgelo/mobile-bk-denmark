import React from "react";
import { ImageBackground } from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";

import SInfo from "react-native-sensitive-info";
import * as _ from "lodash";

import { USER_DATA } from "../../common/values/constants";
import { setLoggedIn } from "../../actions/user";
import { getLinks } from "../../actions/links";
import { setServer } from "../../actions/config";

import styles from "./styles";

class SplashScreen extends React.PureComponent {
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(setServer());
  }

  goToHome = () => {
    const { dispatch } = this.props;
    SInfo.getItem(USER_DATA, {}).then(value => {
      if (!_.isEmpty(value) && !_.isUndefined(value) && !_.isNull(value)) {
        let user = JSON.parse(value);
        dispatch(setLoggedIn(user));
      }

      Actions.replace("home");
    });
  };

  getNecessaryLinks = () => {
    const { dispatch } = this.props;
    dispatch(getLinks(this.goToHome));
  };

  componentDidMount() {
    setTimeout(() => {
      this.getNecessaryLinks();
    }, 300);
  }

  render() {
    return (
      <ImageBackground
        style={styles.container}
        source={require("../../images/bg.png")}
      />
    );
  }
}

export default connect(null)(SplashScreen);

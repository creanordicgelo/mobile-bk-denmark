import { StyleSheet } from "react-native";
import { isIphoneX } from "react-native-iphone-x-helper";
import { Platform } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: isIphoneX() ? 100 : Platform.OS === "ios" ? 80 : 72
  }
});

export default styles;

import React from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { Container, Text, Button } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";

import styles from "./styles";
import textStyles from "../../common/values/textStyles";
import ActionBar from "../../common/components/ActionBar/ActionBar";

import lang from "../../config/strings.json";
import { acceptGDPR } from "../../actions/gdpr";

class Gdpr extends React.PureComponent {
  onAccept = () => {
    acceptGDPR(this.goToHome);
  };

  goToHome = () => {
    Actions.replace("home");
  };

  onDecline = () => {};
  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop:
            isIphoneX() === true ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <View style={styles.container}>
          <Text style={{ color: "#606060", fontWeight: "bold", fontSize: 18 }}>
            {lang.gdpr.title}
          </Text>
          <Text
            style={[
              {
                marginTop: 20,
                fontSize: 14,
                color: "#606060",
                fontWeight: "100"
              }
            ]}
          >
            {this.props.message}
          </Text>

          <View style={{ marginTop: 32, flex: 1, flexDirection: "row" }}>
            <TouchableOpacity onPress={this.onAccept} style={{ flex: 1 }}>
              <ImageBackground
                style={[styles.imgMenu]}
                source={require("../../images/img_gray_button.png")}
              >
                <Text
                  style={[
                    textStyles.txtCalloutStyle,
                    { color: "white", fontWeight: "100", fontSize: 14 }
                  ]}
                >
                  {lang.gdpr.accept}
                </Text>
              </ImageBackground>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={this.onDecline}
              style={{ flex: 1, alignItems: "flex-end" }}
            >
              <ImageBackground
                style={[styles.imgMenu]}
                source={require("../../images/img_gray_button.png")}
              >
                <Text
                  style={[
                    textStyles.txtCalloutStyle,
                    { color: "white", fontWeight: "100", fontSize: 14 }
                  ]}
                >
                  {lang.gdpr.decline}
                </Text>
              </ImageBackground>
            </TouchableOpacity>
          </View>
        </View>

        <ActionBar />
      </ImageBackground>
    );
  }
}

export default Gdpr;

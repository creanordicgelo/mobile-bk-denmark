import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 32,
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 32
  },
  imgMenu: {
    height: 40,
    width: 100,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default styles;

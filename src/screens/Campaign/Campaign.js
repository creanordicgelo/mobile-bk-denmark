import React from "react";
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  RefreshControl,
  FlatList,
  ScrollView,
  Dimensions
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { isIphoneX } from "react-native-iphone-x-helper";
import ImageLoad from "react-native-image-placeholder";
import AutoHeightImage from "react-native-auto-height-image";

import styles from "./styles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import Loading from "../../common/components/Loading/Loading";

import { getCampaigns } from "../../actions/campaigns";

const widthDimension = Dimensions.get("window").width;

class Campaign extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      refreshing: false
    };
  }

  componentDidMount() {
    this.props.dispatch(getCampaigns());
  }

  onRefresh = () => {
    this.setState({ refreshing: true });

    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 300);
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 400;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() === true ? 40 : 20
        }}
        source={require("../../images/bg.png")}
      >
        <ScrollView
          scrollEventThrottle={400}
          onScroll={({ nativeEvent }) => {
            if (this.isCloseToBottom(nativeEvent)) {
              // get other deatails for pagination
            }
          }}
          style={{ paddingTop: 20 }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
        >
          <View style={styles.container}>
            <FlatList
              style={{ flex: 1 }}
              data={this.props.campaigns.data}
              showsVerticalScrollIndicator={false}
              renderItem={({ item, index }) => (
                <View
                  style={{
                    marginTop: 16,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <AutoHeightImage
                    width={widthDimension * 0.9}
                    source={{
                      uri: item.Image
                    }}
                  />
                  <View
                    style={{
                      opacity: 0.5,
                      width: "95%",
                      height: 1,
                      marginTop: 16,
                      backgroundColor: "#606060"
                    }}
                  />
                </View>
              )}
              keyExtractor={item => item._id.toString()}
            />
          </View>
        </ScrollView>
        <ActionBar />
        <Loading isVisible={this.props.isLoading} />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    campaigns: state.campaigns,
    isLoading: state.ui.isLoading
  };
};

export default connect(mapStateToProps)(Campaign);

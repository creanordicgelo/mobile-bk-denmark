import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 32,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 64,
    justifyContent: "center",
    alignItems: "center"
  },
  img: {
    width: widthDimension * 0.9,
    height: widthDimension * 0.9
  }
});

export default styles;

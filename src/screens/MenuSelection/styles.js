import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 32
  },
  imgItem: {
    width: widthDimension * 0.8,
    height: widthDimension * 0.75
  },
  imgFullWidth: {
    width: widthDimension * 0.9,
    height: widthDimension * 0.8
  },
  imgTitle: {
    width: widthDimension * 0.75,
    height: widthDimension * 0.17
  }
});

export default styles;

import React from "react";
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Dimensions
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import ImageLoad from "react-native-image-placeholder";
import * as _ from "lodash";

import styles from "./styles";
import textStyles from "../../common/values/textStyles";

import { devURL } from "../../config/settings";

const widthDimension = Dimensions.get("window").width;

export default class MenuSelectionItem extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isShow: false
    };
  }

  useCloudinaryThumbnails = url => {
    const cloudinary = `https://res.cloudinary.com/burgerking/image/upload/w_${widthDimension},c_limit/v1559731501`;

    let newUrl = url.replace(
      "http://bk-emea-prd.s3.amazonaws.com/sites/burgerking.dk/files",
      cloudinary
    );

    return newUrl;
  };

  renderItem(data) {
    var arr = [];

    Object.keys(data).forEach(function(key) {
      let newData = {
        key: key,
        val: data[key]
      };
      arr.push(newData);
    });

    return (
      <View
        style={{
          marginTop: 8,
          width: "80%"
        }}
      >
        {arr.map((item, index) => (
          <View
            key={index.toString()}
            style={{
              padding: 8,
              flexDirection: "row",
              backgroundColor: index % 2 === 0 ? "silver" : "white"
            }}
          >
            <Text style={{ flex: 1, fontSize: 12 }}>
              {JSON.stringify(item.key).slice(1, -1)}
            </Text>
            <Text style={{ flex: 1, fontSize: 12, textAlign: "right" }}>
              {JSON.stringify(item.val).slice(1, -1)}
            </Text>
          </View>
        ))}
      </View>
    );
  }

  render() {
    let { item } = this.props;
    return (
      <View>
        <TouchableOpacity
          style={{
            marginTop: 16,
            justifyContent: "center",
            alignItems: "center"
          }}
          onPress={() => {
            let { isShow } = this.state;
            this.setState({ isShow: !isShow });
          }}
        >
          <ImageLoad
            resizeMode="contain"
            loadingStyle={{ size: "small", color: "black" }}
            style={styles.imgItem}
            source={{
              uri: this.useCloudinaryThumbnails(item.Image)
            }}
          />

          {!this.state.isShow && item.LabelImage && (
            <ImageLoad
              resizeMode="contain"
              loadingStyle={{ size: "small", color: "black" }}
              style={styles.imgTitle}
              source={{
                uri: this.useCloudinaryThumbnails(item.LabelImage)
              }}
            />
          )}

          {this.state.isShow && (
            <ImageBackground
              style={{
                marginTop: 8,
                paddingBottom: 24,
                width: "100%"
              }}
              resizeMode="stretch"
              source={require("../../images/img_menu_bg.png")}
            >
              <Text
                style={[
                  textStyles.txtMenuHeaderTitle,
                  {
                    width: "85%",
                    color: "red",
                    textAlign: "center",
                    marginTop: 24
                  }
                ]}
              >
                {item.Name.toUpperCase()}
              </Text>

              <FlatList
                data={item.ContainsModified}
                showsVerticalScrollIndicator={false}
                renderItem={({ item, index }) => (
                  <View
                    style={{
                      justifyContent: "center",
                      alignItems: "center",
                      width: "100%"
                    }}
                  >
                    <Text
                      style={[
                        textStyles.txtSubMenuHeaderTitle,
                        {
                          marginRight: 24,
                          color: "white",
                          marginTop: 8,
                          textAlign: "center"
                        }
                      ]}
                    >
                      {item.Title.toUpperCase()}
                    </Text>

                    {this.renderItem(item.Obj)}
                  </View>
                )}
                keyExtractor={item => item.Title}
              />
            </ImageBackground>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

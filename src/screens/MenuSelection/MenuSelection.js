import React from "react";
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  RefreshControl,
  FlatList,
  ScrollView,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { Container, Text, Button } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";
import ImageLoad from "react-native-image-placeholder";

import styles from "./styles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import Loading from "../../common/components/Loading/Loading";
import textStyles from "../../common/values/textStyles";
import MenuSelectionItem from "./MenuSelectionItem";

import { devURL } from "../../config/settings";
import { getMenuByCategory, clearMenu } from "../../actions/menu";

class MenuSelection extends React.PureComponent {
  componentDidMount() {
    let { title } = this.props;
    this.props.dispatch(getMenuByCategory(title));
  }

  componentWillMount() {
    this.props.dispatch(clearMenu());
  }

  onBackClicked = () => {
    Actions.pop();
  };

  render() {
    let { products, title } = this.props;
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() === true ? 40 : 20
        }}
        source={require("../../images/bg.png")}
      >
        {products.length > 0 && (
          <View style={styles.container}>
            <FlatList
              style={{ width: "100%" }}
              data={products}
              showsVerticalScrollIndicator={false}
              renderItem={({ item, index }) => (
                <MenuSelectionItem item={item} />
              )}
              keyExtractor={(item, index) => {
                return item.ProductID.toString() + "_" + index;
              }}
            />
          </View>
        )}

        <ActionBar showBack={true} onBackClicked={this.onBackClicked} />
        <Loading isVisible={this.props.isLoading} />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    products: state.menu.products,
    isLoading: state.ui.isLoading
  };
};

export default connect(mapStateToProps)(MenuSelection);

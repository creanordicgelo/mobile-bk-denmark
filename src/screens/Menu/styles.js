import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const imgMenuWidth = widthDimension * 0.7;
const unitMenu = imgMenuWidth / 26;
const imgMenuHeight = unitMenu * 7;

const unitMenuChicken = imgMenuWidth / 65;
const imgMenuChickenHeight = unitMenuChicken * 16;

const unitMenuBeverage = (widthDimension * 0.65) / 325;
const imgMenuBeverageHeight = unitMenuBeverage * 54;

const unitMenuSnacks = (widthDimension * 0.8) / 50;
const imgMenuSnacksHeight = unitMenuChicken * 11;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    padding: 10,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  imgMenu: {
    width: imgMenuWidth,
    height: imgMenuHeight
  },
  imgMenuChicken: {
    width: imgMenuWidth,
    height: imgMenuChickenHeight
  },
  imgMenuBeverage: {
    width: widthDimension * 0.65,
    height: imgMenuBeverageHeight
  },
  imgMenuSnacks: {
    width: widthDimension * 0.7,
    height: (widthDimension * 0.8) / 4
  },
  imgMenu2: {
    width: widthDimension * 0.7,
    height: (widthDimension * 0.8) / 4
  },
  imgMeal: {
    width: widthDimension * 0.85,
    height: (widthDimension * 0.7) / 4,
    marginTop: 8
  },
  imgSlogan: {
    width: widthDimension * 0.55,
    height: (widthDimension * 0.55) / 4
  },
  touchableStyle: {
    width: "100%",
    alignItems: "center"
  }
});

export default styles;

import React from "react";
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  Dimensions
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { Container, Text, Button } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";

import styles from "./styles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import textStyles from "../../common/values/textStyles";
import { getMenuByCategory, clearMenu } from "../../actions/menu";
import lang from "../../config/strings.json";

const widthDimension = Dimensions.get("window").width;

class Menu extends React.Component {
  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop:
            isIphoneX() === true ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <View style={styles.container}>
          <TouchableOpacity
            style={styles.touchableStyle}
            onPress={() => {
              Actions.menuSelection({ title: lang.bk_menu.burgers });
            }}
          >
            <Image
              resizeMode={"contain"}
              style={[styles.imgMenu, { marginLeft: 30 }]}
              source={require("../../images/menu_item_burgers.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touchableStyle}
            onPress={() => {
              Actions.menuSelection({ title: lang.bk_menu.chicken });
            }}
          >
            <Image
              resizeMode={"contain"}
              style={[
                styles.imgMenuChicken,
                { marginRight: 30, width: widthDimension * 0.75 }
              ]}
              source={require("../../images/menu_item_chicken_and_more.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touchableStyle}
            onPress={() => {
              Actions.menuSelection({ title: lang.bk_menu.drinks });
            }}
          >
            <Image
              resizeMode={"contain"}
              style={[styles.imgMenuBeverage]}
              source={require("../../images/menu_item_drinks_and_hot_beverages.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touchableStyle}
            onPress={() => {
              Actions.menuSelection({ title: lang.bk_menu.snacks });
            }}
          >
            <Image
              resizeMode={"contain"}
              style={[
                styles.imgMenuSnacks,
                { marginLeft: 20, width: widthDimension * 0.75 }
              ]}
              source={require("../../images/menu_item_snacks.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touchableStyle}
            onPress={() => {
              Actions.menuSelection({ title: lang.bk_menu.deserts });
            }}
          >
            <Image
              resizeMode={"contain"}
              style={[
                styles.imgMenu,
                { marginRight: 30, height: (widthDimension * 0.6) / 4 }
              ]}
              source={require("../../images/menu_item_desserts.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touchableStyle}
            onPress={() => {
              Actions.menuSelection({ title: lang.bk_menu.veggie });
            }}
          >
            <Image
              resizeMode={"contain"}
              style={[
                styles.imgMenu2,
                {
                  marginLeft: 30,
                  height: (widthDimension * 0.6) / 4
                }
              ]}
              source={require("../../images/menu_item_veggie.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touchableStyle}
            onPress={() => {
              Actions.menuSelection({ title: lang.bk_menu.king_jr });
            }}
          >
            <Image
              resizeMode={"contain"}
              style={[
                {
                  width: widthDimension * 0.73,
                  height: (widthDimension * 0.5) / 4
                }
              ]}
              source={require("../../images/menu_item_king_jr_meals.png")}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.touchableStyle}
            onPress={() => {
              Actions.menuSelection({ title: lang.bk_menu.dip });
            }}
          >
            <Image
              resizeMode={"contain"}
              style={[
                styles.imgMenu2,
                {
                  marginLeft: 30,
                  width: widthDimension * 0.7,
                  height: (widthDimension * 0.6) / 4
                }
              ]}
              source={require("../../images/menu_item_dip.png")}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flex: 3,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Image
            style={styles.imgSlogan}
            source={require("../../images/img_slogan.png")}
          />
        </View>

        <ActionBar />
      </ImageBackground>
    );
  }
}

export default Menu;

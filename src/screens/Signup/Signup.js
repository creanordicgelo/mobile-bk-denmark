import React from "react";
import { View, ImageBackground, TouchableOpacity, Image } from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { Container, Text, Button } from "native-base";

import styles from "./styles";
import textStyles from "../../common/values/textStyles";
import renderIf from "../../common/utils/renderIf";

import lang from "../../config/strings.json";

class Home extends React.Component {
  render() {
    return (
      <Container style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            Actions.pop();
          }}
        >
          <Text> {lang.signup.signup} </Text>
        </TouchableOpacity>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.reducer.data,
    ui: state.ui
  };
};

export default connect(mapStateToProps)(Home);

import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 32,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default styles;

import React from "react";
import { View, ImageBackground, ScrollView, Platform } from "react-native";
import { Actions } from "react-native-router-flux";
import { Text } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";

import styles from "./styles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import textStyles from "../../common/values/textStyles";

import lang from "../../config/strings.json";

class Feedback extends React.PureComponent {
  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop:
            isIphoneX() === true ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <ScrollView>
          <View style={styles.container}>
            <Text style={textStyles.txtMenuTitle}>
              {lang.feedback.feedback}
            </Text>
          </View>
        </ScrollView>
        <ActionBar />
      </ImageBackground>
    );
  }
}

export default Feedback;

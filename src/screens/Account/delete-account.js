import React from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  Platform
} from "react-native";
import { Actions } from "react-native-router-flux";
import { Container, Text } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";

import Footer from "./footer";
import styles from "./styles";

import textStyles from "../../common/values/textStyles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import Crown from "../../common/components/Crown";
import DeleteButton from "../../common/components/BkButton";
import constants from "../../common/values";

import lang from "../../config/strings.json";

class DeleteAccount extends React.PureComponent {
  onDoneClick = () => {};

  onDeleteAccount = () => {};

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <View style={styles.container}>
          <Crown
            title={lang.crown_title.slet_mit_bk}
            crownType={constants.crownType.black}
          />

          <View
            style={{
              marginTop: 48,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <DeleteButton
              onActionClick={this.onDeleteAccount}
              title={lang.account.delete_click}
              theme="#404042"
              fontAttr={18}
            />
          </View>

          <Text
            style={[
              styles.txtDescription,
              {
                marginTop: 32,
                paddingLeft: 68,
                paddingRight: 68
              }
            ]}
          >
            {lang.account.delete_info}
          </Text>
        </View>
        <Footer hasDone={true} hasBack={true} onDoneClick={this.onDoneClick} />
        <ActionBar />
      </ImageBackground>
    );
  }
}

export default DeleteAccount;

import React from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  Platform,
  Linking
} from "react-native";
import { Actions } from "react-native-router-flux";
import { Container, Text, CheckBox } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DialogBox from "react-native-dialogbox";
import { connect } from "react-redux";
import * as _ from "lodash";
import moment from "moment";

import Footer from "./footer";
import styles from "./styles";

import textStyles from "../../common/values/textStyles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import BkTextInput from "../../common/components/BkTextInput";
import BkRadioButton from "../../common/components/BKRadioButton";
import BKDropDown from "../../common/components/BKDropDown";
import Crown from "../../common/components/Crown";
import constants from "../../common/values";
import { userRegister } from "../../actions/user";
import Loading from "../../common/components/Loading/Loading";

import lang from "../../config/strings.json";

class CreateProfile extends React.Component {
  constructor(props) {
    super(props);
    const startDate = moment();
    const endDate = moment().subtract(100, "year");

    const years = moment(startDate).diff(endDate, "years");
    let yearsBetween = [];
    for (let year = 0; year < years; year++) {
      let dateAdded = moment(startDate)
        .subtract(year, "year")
        .format("Y");
      yearsBetween.push({ value: dateAdded, text: dateAdded });
    }

    let value = yearsBetween[0].value;

    this.state = {
      txtUserName: "",
      txtEmail: "",
      txtPassword: "",
      txtConfirmPassword: "",
      sexData: [false, false, false],
      yearsBetween,
      skip_bday: false,
      value,
      acceptPolicy: false
    };
  }

  componentDidUpdate(prevProps) {
    const { loginType, isLoggedIn } = this.props;
    if (isLoggedIn && loginType === "register") {
      Actions.reset("createdAccount");
    }
  }

  onDoneClick = () => {
    const {
      txtUserName,
      txtEmail,
      txtPassword,
      txtConfirmPassword,
      sexData,
      skip_bday,
      value,
      acceptPolicy
    } = this.state;

    const index = _.findIndex(this.state.sexData, object => object === true);
    let gender = "";
    if (index === 0) {
      gender = "male";
    } else if (index === 1) {
      gender = "female";
    }

    if (
      _.isEmpty(txtUserName) ||
      _.isEmpty(txtEmail) ||
      _.isEmpty(txtPassword) ||
      _.isEmpty(txtConfirmPassword) ||
      index === -1
    ) {
      this.onMissingInformation();
      return;
    } else if (txtPassword != txtConfirmPassword) {
      this.onPasswordDoesNotMatch();
      return;
    } else if (acceptPolicy === false) {
      this.onNotAcceptPolicy();
      return;
    }

    const birthYear = skip_bday === true ? "" : value;

    this.props.dispatch(
      userRegister(
        txtUserName,
        txtPassword,
        txtEmail,
        gender,
        birthYear,
        this.onErrorRegistrationCallback
      )
    );
  };

  onErrorRegistrationCallback = message => {
    if (message) {
      this.dialogbox.alert(lang.error.title.registration, message);
    }
  };

  onMissingInformation = () => {
    this.dialogbox.alert(
      lang.error.title.registration,
      lang.error.desc.missing_information
    );
  };

  onNotAcceptPolicy = () => {
    this.dialogbox
      .alert(lang.error.title.registration, lang.error.accept_terms)
      .then(() => {
        this.scroll.scrollToEnd();
      });
  };

  onPasswordDoesNotMatch = () => {
    this.dialogbox.alert(
      lang.error.title.registration,
      lang.error.desc.password_not_match
    );
  };

  onBackClick = () => {
    Actions.pop();
  };

  onUpdateEmailValue = text => {
    this.setState({
      txtEmail: text
    });
  };

  onUpdateUserNameValue = text => {
    this.setState({
      txtUserName: text
    });
  };

  onUpdatePasswordValue = text => {
    this.setState({
      txtPassword: text
    });
  };

  onUpdateConfirmPasswordValue = text => {
    this.setState({
      txtConfirmPassword: text
    });
  };

  onSelectGender = sexPref => {
    let sexData = [false, false, false];
    sexData[sexPref] = true;

    this.setState({ sexData });
  };

  openLink = url => {
    Linking.openURL(url).catch(err => console.error("An error occurred", err));
  };

  render() {
    let { txtUserName, txtEmail, txtPassword, txtConfirmPassword } = this.state;
    const { termandpolicy } = this.props.links;
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <KeyboardAwareScrollView
          style={styles.container}
          contentContainerStyle={{ paddingBottom: 60 }}
          ref={ref => (this.scroll = ref)}
          behavior="padding"
          extraScrollHeight={64}
          keyboardShouldPersistTaps="handled"
        >
          <Crown
            title={lang.crown_title.opret_profil}
            crownType={constants.crownType.yellow}
          />

          <View>
            <View style={{ marginTop: 16 }}>
              <Text style={[styles.txtInfoTitle]}>
                {lang.account.user_name}
              </Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdateUserNameValue}
                  placeHolder={lang.account.enter_user_name}
                  textInput={txtUserName}
                />
              </View>
            </View>

            <View style={{ marginTop: 16 }}>
              <Text style={[styles.txtInfoTitle]}>{lang.account.email}</Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdateEmailValue}
                  placeHolder={lang.account.enter_email}
                  textInput={txtEmail}
                />
              </View>
            </View>

            <View style={{ marginTop: 40 }}>
              <Text style={[styles.txtInfoTitle]}>
                {lang.account.choose_password}
              </Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdatePasswordValue}
                  placeHolder={lang.account.enter_password}
                  secureTextEntry
                  textInput={txtPassword}
                />
              </View>
            </View>

            <View style={{ marginTop: 16 }}>
              <Text style={[styles.txtInfoTitle]}>
                {lang.account.repeat_password}
              </Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdateConfirmPasswordValue}
                  placeHolder={lang.account.enter_password}
                  secureTextEntry
                  textInput={txtConfirmPassword}
                />
              </View>
            </View>
            <View style={{ marginTop: 16, width: "100%" }}>
              <Text style={[styles.txtInfoTitle]}>{lang.account.sex}</Text>

              <View
                style={{
                  marginTop: 8,
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <Text style={[styles.txtRadioTitle]}>{lang.account.man}</Text>

                  <BkRadioButton
                    selected={this.state.sexData[0]}
                    size={20}
                    onActionClick={() => {
                      this.onSelectGender(0);
                    }}
                    theme={"#f8b016"}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <Text style={[styles.txtRadioTitle]}>
                    {lang.account.woman}
                  </Text>

                  <BkRadioButton
                    selected={this.state.sexData[1]}
                    size={20}
                    onActionClick={() => {
                      this.onSelectGender(1);
                    }}
                    theme={"#f8b016"}
                  />
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <Text style={[styles.txtRadioTitle]}>
                    {lang.account.dont_inform}
                  </Text>

                  <BkRadioButton
                    selected={this.state.sexData[2]}
                    size={20}
                    onActionClick={() => {
                      this.onSelectGender(2);
                    }}
                    theme={"#f8b016"}
                  />
                </View>
              </View>
            </View>
          </View>

          <View style={{ marginTop: 16 }}>
            <Text style={[styles.txtInfoTitle]}>
              {lang.account.year_of_birth}
            </Text>

            <View
              style={{
                marginTop: 8,
                flexDirection: "row",
                justifyContent: "space-evenly"
              }}
            >
              <View style={{ flex: 1, paddingRight: 20 }}>
                <BKDropDown
                  dataParams={this.state.yearsBetween}
                  value={this.state.value}
                  fontStyle={styles.txtDropdown}
                  onChange={value => {
                    this.setState({ value, skip_bday: false });
                  }}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Text style={[styles.txtRadioTitle]}>
                  {lang.account.dont_inform}
                </Text>

                <BkRadioButton
                  selected={this.state.skip_bday}
                  size={20}
                  onActionClick={() => {
                    const skip_bday = !this.state.skip_bday;
                    this.setState({ skip_bday });
                  }}
                  theme={"#f8b016"}
                />
              </View>
            </View>
          </View>

          <View
            style={{
              marginTop: 24,
              marginRight: 8,
              width: "100%"
            }}
          >
            <Text style={[styles.txtInfoTitle]}>
              {lang.account.terms_of_service_title}
            </Text>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <View style={{ flex: 1, marginRight: 8, marginTop: 16 }}>
                <Text
                  style={{
                    fontSize: 10,
                    color: "silver"
                  }}
                >
                  {lang.account.policy_disclaimer_1a.toUpperCase()}{" "}
                  <Text
                    style={{
                      color: "#47c3f4",
                      fontSize: 12,
                      textDecorationLine: "underline"
                    }}
                    onPress={() => {
                      this.openLink(termandpolicy);
                    }}
                  >
                    {lang.account.policy_disclaimer_1link.toUpperCase()}
                  </Text>
                  {lang.account.policy_disclaimer_1b.toUpperCase()}
                </Text>

                <Text
                  style={{
                    marginTop: 16,
                    fontSize: 10,
                    color: "silver"
                  }}
                >
                  {lang.account.policy_disclaimer_2.toUpperCase()}{" "}
                  <Text
                    style={{
                      color: "#47c3f4",
                      fontSize: 10,
                      textDecorationLine: "underline"
                    }}
                    onPress={() => {
                      this.openLink(termandpolicy);
                    }}
                  >
                    {lang.account.policy_disclaimer_2link.toUpperCase()}
                  </Text>
                </Text>
              </View>
              <BkRadioButton
                selected={this.state.acceptPolicy}
                size={20}
                onActionClick={() => {
                  this.setState({ acceptPolicy: !this.state.acceptPolicy });
                }}
                theme={"#f8b016"}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
        <Footer hasDone={true} onDoneClick={this.onDoneClick} />
        <ActionBar showBack={true} onBackClick={this.onBackClick} />
        <DialogBox
          ref={ref => {
            this.dialogbox = ref;
          }}
        />
        <Loading isVisible={this.props.isLoading} />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    isLoggedIn: state.user.isLoggedIn,
    links: state.links.links,
    loginType: state.user.loginType
  };
};

export default connect(mapStateToProps)(CreateProfile);

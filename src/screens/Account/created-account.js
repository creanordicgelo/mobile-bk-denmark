import React from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  Platform,
  BackHandler
} from "react-native";
import { Actions } from "react-native-router-flux";
import { Container, Text } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";
import { connect } from "react-redux";

import Footer from "./footer";
import styles from "./styles";

import textStyles from "../../common/values/textStyles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import Crown from "../../common/components/Crown";
import constants from "../../common/values";

import lang from "../../config/strings.json";

class CreatedAccount extends React.PureComponent {
  onDoneClick = () => {
    const { loginLocation } = this.props;
    if (loginLocation === "home") {
      Actions.reset("home");
    } else if (loginLocation === "coupons") {
      Actions.reset("coupons");
    }
  };

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      Actions.replace("home");
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <ScrollView style={styles.container}>
          <Crown
            title={lang.crown_title.profil_oprettet}
            crownType={constants.crownType.yellow}
          />

          <Text
            style={[
              textStyles.txtCalloutStyle,
              {
                marginTop: 72,
                color: "#f8b016",
                fontSize: 28,
                paddingLeft: 16,
                paddingRight: 16,
                textAlign: "center"
              }
            ]}
          >
            {lang.account.welcome_to_bk}
          </Text>

          <Text
            style={[
              textStyles.txtCalloutStyle,
              {
                marginTop: 40,
                color: "#f8b016",
                fontSize: 28,
                textAlign: "center"
              }
            ]}
          >
            {lang.account.thanks}
          </Text>
        </ScrollView>
        <Footer hasDone={true} onDoneClick={this.onDoneClick} />
        <ActionBar />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginLocation: state.user.loginLocation
  };
};

export default connect(mapStateToProps)(CreatedAccount);

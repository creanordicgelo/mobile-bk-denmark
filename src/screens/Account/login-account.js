import React from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  Platform
} from "react-native";
import { Actions } from "react-native-router-flux";
import { Container, Text } from "native-base";
import { connect } from "react-redux";
import { isIphoneX } from "react-native-iphone-x-helper";
import * as _ from "lodash";
import DialgBox from "react-native-dialogbox";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager
} from "react-native-fbsdk";
import Footer from "./footer";
import styles from "./styles";

import textStyles from "../../common/values/textStyles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import BkTextInput from "../../common/components/BkTextInput";
import Crown from "../../common/components/Crown";
import Loading from "../../common/components/Loading/Loading";
import constants from "../../common/values";
import { userLogin, loginCreateViaFB } from "../../actions/user";

import lang from "../../config/strings.json";

class LoginAccount extends React.Component {
  state = { txtEmail: "", txtPassword: "", accessToken: "" };

  componentDidUpdate(prevProps) {
    const { loginType, loginLocation, isLoggedIn } = this.props;
    if (isLoggedIn && (loginType === "login" || loginType === "fb")) {
      if (loginLocation === "home") {
        Actions.reset("home");
      } else if (loginLocation === "coupons") {
        Actions.reset("coupons");
      }
    }
  }

  onDoneClick = () => {
    const { txtEmail, txtPassword } = this.state;

    if (_.isEmpty(txtEmail) || _.isEmpty(txtPassword)) {
      this.onMissingInformation();
      return;
    }

    this.props.dispatch(
      userLogin(
        txtEmail,
        txtPassword,
        "login",
        this.onErrorRegistrationCallback
      )
    );
  };

  onErrorRegistrationCallback = message => {
    if (message) {
      this.dialogbox.alert(lang.error.title.registration, message);
    }
  };

  onMissingInformation = () => {
    this.dialogbox.alert(lang.error.title.login, lang.error.desc.login_missing);
  };

  //Create response callback.
  _responseInfoCallback = (error: ?Object, result: ?Object) => {
    if (error) {
      // error
    } else {
      const fbId = result.id;
      this.props.dispatch(loginCreateViaFB(this.state.accessToken, fbId));
    }
  };

  onInvalidLogin = () => {
    this.dialogbox
      .alert(lang.error.title.login, lang.error.desc.login_invalid)
      .then(() => {
        this.setState({ txtEmail: "", txtPassword: "" });
      });
  };

  onBackClick = () => {
    Actions.pop();
  };

  onForgotPasswordClick = () => {
    this.setState({ txtEmail: "", txtPassword: "" });
    Actions.forgotPassword();
  };

  onCreateUserEmail = () => {
    this.setState({ txtEmail: "", txtPassword: "" });
    Actions.createProfile();
  };

  requestUserFBDetails = accessToken => {
    const infoRequest = new GraphRequest(
      "/me",
      null,
      this._responseInfoCallback
    );

    this.setState({ accessToken });

    // Start the graph request.
    new GraphRequestManager().addRequest(infoRequest).start();
  };

  onCreateUserFb = () => {
    const selfRef = this;
    this.setState({ txtEmail: "", txtPassword: "" });
    LoginManager.logInWithPermissions(["public_profile", "email"]).then(
      function(result) {
        if (result.isCancelled) {
          // cancelled
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            selfRef.requestUserFBDetails(data.accessToken.toString());
          });
        }
      },
      function(error) {
        // error
      }
    );
  };

  onUpdateEmailValue = text => {
    this.setState({
      txtEmail: text
    });
  };

  onUpdatePasswordValue = text => {
    this.setState({
      txtPassword: text
    });
  };

  render() {
    let { txtEmail, txtPassword } = this.state;
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <KeyboardAwareScrollView
          style={styles.container}
          behavior="padding"
          extraScrollHeight={64}
          keyboardShouldPersistTaps="handled"
        >
          <Crown
            title={lang.crown_title.mit_bk}
            crownType={constants.crownType.yellow}
          />

          <View style={{ marginTop: 16 }}>
            <Text style={[textStyles.txtCalloutStyle, styles.txtTitle]}>
              {lang.account.login}
            </Text>

            <View style={{ marginTop: 16 }}>
              <Text style={[styles.txtInfoTitle]}>
                {lang.account.email_or_username}
              </Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdateEmailValue}
                  placeHolder={lang.account.enter_email_or_username}
                  textInput={txtEmail}
                />
              </View>
            </View>

            <View style={{ marginTop: 16 }}>
              <Text style={[styles.txtInfoTitle]}>{lang.account.password}</Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdatePasswordValue}
                  placeHolder={lang.account.enter_password}
                  secureTextEntry={true}
                  textInput={txtPassword}
                />
              </View>
            </View>

            <TouchableOpacity
              onPress={() => {
                this.onForgotPasswordClick();
              }}
              style={{ marginTop: 16 }}
            >
              <Text
                style={[
                  styles.txtDescription,
                  { color: "gray", textAlign: "right", fontWeight: "100" }
                ]}
              >
                {lang.account.forgot_password}
              </Text>
            </TouchableOpacity>

            <Text
              style={[
                textStyles.txtCalloutStyle,
                styles.txtTitle,
                { marginTop: 32 }
              ]}
            >
              {lang.account.not_registered}
            </Text>

            <Text
              style={[
                styles.txtDescription,
                {
                  marginTop: 16
                }
              ]}
            >
              {lang.account.not_registered_info}
            </Text>

            <TouchableOpacity
              style={{ marginTop: 24, position: "relative" }}
              onPress={() => {
                this.onCreateUserEmail();
              }}
            >
              <ImageBackground
                style={{
                  flex: 1,
                  height: 40
                }}
                source={require("../../images/bg_input.png")}
              />

              <Text
                style={[
                  textStyles.txtCalloutStyle,
                  {
                    position: "absolute",
                    textAlign: "center",
                    color: "white",
                    fontSize: 18,
                    marginTop: 8,
                    left: 0,
                    right: 0
                  }
                ]}
              >
                {lang.account.create_new_user_email}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{ marginTop: 16, marginBottom: 40, position: "relative" }}
              onPress={() => {
                this.onCreateUserFb();
              }}
            >
              <ImageBackground
                style={{
                  flex: 1,
                  height: 40
                }}
                source={require("../../images/bg_fb.png")}
              />

              <Text
                style={[
                  textStyles.txtCalloutStyle,
                  {
                    position: "absolute",
                    textAlign: "center",
                    left: 0,
                    right: 0,
                    color: "white",
                    fontSize: 18,
                    marginTop: 8
                  }
                ]}
              >
                {lang.account.create_new_user_fb}
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        <Footer hasDone={true} onDoneClick={this.onDoneClick} />
        <ActionBar showBack={true} onBackClick={this.onBackClick} />
        <DialgBox
          ref={ref => {
            this.dialogbox = ref;
          }}
        />
        <Loading isVisible={this.props.isLoading} />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    error: state.user.error,
    user: state.user.user,
    isLoggedIn: state.user.isLoggedIn,
    loginType: state.user.loginType,
    loginLocation: state.user.loginLocation
  };
};

export default connect(mapStateToProps)(LoginAccount);

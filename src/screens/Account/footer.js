import React from "react";
import PropTypes from "prop-types";
import { View, Text, Image } from "react-native";
import { isIphoneX } from "react-native-iphone-x-helper";

import ActionButton from "../../common/components/BkButton";
import constants from "../../common/values";
import textStyles from "../../common/values/textStyles";

import lang from "../../config/strings.json";

const Footer = ({ onBackClick, hasBack, onDoneClick, hasDone }) => (
  <View>
    {hasBack && (
      <View
        style={{
          position: "absolute",
          bottom: isIphoneX() ? 48 : 32,
          left: 32
        }}
      >
        <ActionButton
          onActionClick={() => {
            onBackClick();
          }}
          title={lang.button.back}
          theme="#e2a170"
          fontAttr={24}
        />
      </View>
    )}

    {hasDone && (
      <View
        style={{
          position: "absolute",
          bottom: isIphoneX() ? 48 : 32,
          right: 32
        }}
      >
        <ActionButton
          onActionClick={() => {
            onDoneClick();
          }}
          title={lang.button.done}
          theme="#f8b016"
          fontAttr={24}
        />
      </View>
    )}
  </View>
);

Footer.propTypes = {
  onBackClick: PropTypes.func,
  onDoneClick: PropTypes.func,
  hasBack: PropTypes.bool,
  hasDone: PropTypes.bool
};

Footer.defaultProps = {
  onBackClick: () => {},
  onDoneClick: () => {},
  hasBack: false,
  hasDone: false
};

export default Footer;

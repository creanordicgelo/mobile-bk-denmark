import React from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  Platform
} from "react-native";
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import { Container, Text } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";
import Loading from "../../common/components/Loading/Loading";
import DialogBox from "react-native-dialogbox";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import * as _ from "lodash";

import Footer from "./footer";
import styles from "./styles";

import textStyles from "../../common/values/textStyles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import BkTextInput from "../../common/components/BkTextInput";
import Crown from "../../common/components/Crown";
import constants from "../../common/values";
import { forgotPassword } from "../../actions/user";

import lang from "../../config/strings.json";

class ForgotPassword extends React.PureComponent {
  state = { textInput: "" };

  onDoneClick = () => {
    const { textInput } = this.state;

    if (_.isEmpty(textInput)) {
      this.onMissingInformation();
      return;
    }

    forgotPassword(textInput, this.onForgotPasswordCallback);
  };

  onForgotPasswordCallback = isSuccess => {
    if (isSuccess === true) {
      this.dialogbox.alert("", lang.success.desc.forgot_password);
    } else {
      this.dialogbox.alert(
        lang.error.title.forgot_password,
        lang.error.desc.resetting_issue
      );
    }
  };

  onMissingInformation = () => {
    this.dialogbox.alert(
      lang.error.title.forgot_password,
      lang.error.desc.missing_email
    );
  };

  onBackClick = () => {
    Actions.pop();
  };

  onUpdateInputValue = text => {
    this.setState({
      textInput: text
    });
  };

  render() {
    let { textInput } = this.state;
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <KeyboardAwareScrollView
          style={styles.container}
          behavior="padding"
          extraScrollHeight={64}
          keyboardShouldPersistTaps="handled"
        >
          <Crown
            title={lang.crown_title.glemt_kodeord}
            crownType={constants.crownType.yellow}
          />

          <View style={{ marginTop: 16, paddingLeft: 16, paddingRight: 16 }}>
            <View style={{ marginTop: 16 }}>
              <Text style={[styles.txtInfoTitle]}>{lang.account.email}</Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdateInputValue}
                  placeHolder={lang.account.enter_email}
                  textInput={textInput}
                />
              </View>
            </View>
          </View>

          <Text
            style={[
              styles.txtDescription,
              {
                marginTop: 24,
                paddingLeft: 16,
                paddingRight: 16
              }
            ]}
          >
            {lang.account.reset_password_info}
          </Text>
        </KeyboardAwareScrollView>
        <Footer hasDone={true} onDoneClick={this.onDoneClick} />
        <ActionBar showBack={true} onBackClick={this.onBackClick} />
        <DialogBox
          ref={ref => {
            this.dialogbox = ref;
          }}
        />
        <Loading isVisible={this.props.isLoading} />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    error: state.user.error
  };
};

export default connect(mapStateToProps)(ForgotPassword);

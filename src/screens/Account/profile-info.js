import React from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  Platform,
  Alert,
  Linking
} from "react-native";
import { Actions } from "react-native-router-flux";
import { Container, Text } from "native-base";
import { connect } from "react-redux";
import * as _ from "lodash";
import { isIphoneX } from "react-native-iphone-x-helper";
import DialogBox from "react-native-dialogbox";

import Footer from "./footer";
import styles from "./styles";

import textStyles from "../../common/values/textStyles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import Crown from "../../common/components/Crown";
import ActionButton from "../../common/components/BkButton";
import constants from "../../common/values";
import { userLogout, userDelete } from "../../actions/user";

import lang from "../../config/strings.json";

class ProfileInfo extends React.Component {
  componentDidUpdate(prevProps) {
    if (prevProps.isLoggedIn && !this.props.isLoggedIn) {
      Actions.reset("home");
    }
  }

  onEditProfileClick = () => {
    Actions.editProfile();
  };

  onLogoutClick = () => {
    this.dialogbox.confirm({
      content: lang.account.logout_account_question,
      ok: {
        text: lang.button.yes,
        callback: () => {
          const { user } = this.props;
          this.props.dispatch(userLogout(user.token));
        }
      },
      cancel: {
        text: lang.button.no
      }
    });
  };

  onDeleteProfleClick = () => {
    const { user } = this.props;

    this.dialogbox.confirm({
      content: lang.account.delete_account_question,
      ok: {
        text: lang.button.yes,
        callback: () => {
          this.props.dispatch(userDelete(user.token));
        }
      },
      cancel: {
        text: lang.button.no
      }
    });
  };

  onTermOfServiceClick = () => {
    const { termandpolicy } = this.props.links;
    this.openLink(termandpolicy);
  };

  onDoneClick = () => {};

  onBackClick = () => {
    Actions.pop();
  };

  openLink = url => {
    Linking.openURL(url).catch(err => console.error("An error occurred", err));
  };

  render() {
    const { user, isLoggedIn } = this.props;

    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop:
            isIphoneX() === true ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <ScrollView style={styles.container}>
          <Crown
            title={lang.crown_title.min_profil}
            crownType={constants.crownType.yellow}
          />

          <View style={{ marginTop: 16, paddingLeft: 16, paddingRight: 16 }}>
            <View style={{ marginTop: 16 }}>
              <Text style={[styles.txtInfoTitle]}>
                {lang.account.user_name}
              </Text>

              <Text
                style={[
                  textStyles.txtCalloutStyle,
                  styles.txtInfo,
                  { marginTop: 4 }
                ]}
              >
                {isLoggedIn === true ? user.profile.Username : ""}
              </Text>
            </View>

            <View style={{ marginTop: 12 }}>
              <Text style={[styles.txtInfoTitle]}>{lang.account.email}</Text>

              <Text
                style={[
                  textStyles.txtCalloutStyle,
                  styles.txtInfo,
                  { marginTop: 4 }
                ]}
              >
                {isLoggedIn === true ? user.profile.Email : ""}
              </Text>
            </View>

            <View style={{ marginTop: 12, flexDirection: "row" }}>
              <View style={{ flex: 1, marginRight: 4 }}>
                <Text style={[styles.txtInfoTitle]}>{lang.account.sex}</Text>

                <Text
                  style={[
                    textStyles.txtCalloutStyle,
                    styles.txtInfo,
                    { marginTop: 4 }
                  ]}
                >
                  {isLoggedIn === false || _.isEmpty(user.profile.Gender)
                    ? "N/A"
                    : user.profile.Gender.toLowerCase() === "male"
                    ? "MAND"
                    : "KVINDE"}
                </Text>
              </View>

              <View style={{ flex: 1, marginLeft: 4 }}>
                <Text style={[styles.txtInfoTitle]}>
                  {lang.account.year_of_birth}
                </Text>

                <Text
                  style={[
                    textStyles.txtCalloutStyle,
                    styles.txtInfo,
                    { marginTop: 4 }
                  ]}
                >
                  {isLoggedIn === false || _.isEmpty(user.profile.Birthyear)
                    ? "N/A"
                    : user.profile.Birthyear}
                </Text>
              </View>
            </View>

            <View style={{ marginTop: 8, flexDirection: "row" }}>
              <View style={{ flex: 1, marginRight: 4, marginTop: 8 }}>
                <Text style={[styles.txtInfoTitle]}>
                  {lang.account.settings}
                </Text>

                <View style={{ marginTop: 8, alignSelf: "flex-start" }}>
                  <ActionButton
                    onActionClick={() => this.onEditProfileClick()}
                    title={lang.account.edit_profile}
                    theme="#404042"
                    fontAttr={18}
                  />
                </View>
              </View>

              <View style={{ flex: 1, marginLeft: 4, marginTop: 8 }}>
                <Text style={[styles.txtInfoTitle]}>
                  {lang.account.fine_print}
                </Text>

                <View style={{ marginTop: 8, alignSelf: "flex-start" }}>
                  <ActionButton
                    onActionClick={() => this.onTermOfServiceClick()}
                    title={lang.account.terms_of_service}
                    theme="#404042"
                    fontAttr={18}
                  />
                </View>
              </View>
            </View>

            <View style={{ marginTop: 24, alignSelf: "flex-start" }}>
              <ActionButton
                onActionClick={() => this.onLogoutClick()}
                title={lang.account.log_out}
                theme="#404042"
                fontAttr={18}
              />
            </View>

            <View
              style={{
                marginTop: 24,
                marginBottom: 40,
                alignSelf: "flex-start"
              }}
            >
              <ActionButton
                onActionClick={() => this.onDeleteProfleClick()}
                title={lang.account.delete_profile}
                theme="#404042"
                fontAttr={18}
              />
            </View>
          </View>
        </ScrollView>

        <Footer hasDone={false} onDoneClick={this.onDoneClick} />
        <ActionBar showBack={true} onBackClick={this.onBackClick} />
        <DialogBox
          ref={ref => {
            this.dialogbox = ref;
          }}
        />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.user.isLoggedIn,
    user: state.user.user,
    links: state.links.links
  };
};

export default connect(mapStateToProps)(ProfileInfo);

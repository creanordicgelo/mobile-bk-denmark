import React from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  Image,
  Platform
} from "react-native";
import { Actions } from "react-native-router-flux";
import { Container, Text } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";
import { connect } from "react-redux";
import * as _ from "lodash";
import moment from "moment";
import DialogBox from "react-native-dialogbox";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import Footer from "./footer";
import styles from "./styles";

import textStyles from "../../common/values/textStyles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import BkTextInput from "../../common/components/BkTextInput";
import BkRadioButton from "../../common/components/BKRadioButton";
import BKDropDown from "../../common/components/BKDropDown";
import Crown from "../../common/components/Crown";
import constants from "../../common/values";
import Loading from "../../common/components/Loading/Loading";
import { userEdit } from "../../actions/user";

import lang from "../../config/strings.json";

class EditProfile extends React.Component {
  constructor(props) {
    super(props);

    const startDate = moment();
    const endDate = moment().subtract(100, "year");

    const years = moment(startDate).diff(endDate, "years");
    let yearsBetween = [];
    for (let year = 0; year < years; year++) {
      let dateAdded = moment(startDate)
        .subtract(year, "year")
        .format("Y");
      yearsBetween.push({ value: dateAdded, text: dateAdded });
    }

    let value = _.isEmpty(props.user.profile.Birthyear)
      ? yearsBetween[0].value
      : props.user.profile.Birthyear;

    const gender = _.isUndefined(props.user.profile.Gender)
      ? ""
      : props.user.profile.Gender.toLowerCase();
    const sexData = [gender === "male", gender === "female", gender === ""];
    this.state = {
      txtUserName: props.user.profile.Username,
      txtPassword: "",
      txtConfirmPassword: "",
      sexData,
      skip_bday: _.isEmpty(props.user.profile.Birthyear),
      yearsBetween,
      value
    };
  }

  onDoneClick = () => {
    const {
      txtUserName,
      txtPassword,
      txtConfirmPassword,
      sexData,
      skip_bday,
      value
    } = this.state;

    const index = _.findIndex(this.state.sexData, object => object === true);
    let gender = "";
    if (index === 0) {
      gender = "male";
    } else if (index === 1) {
      gender = "female";
    }

    if (_.isEmpty(txtUserName) || index === -1) {
      this.onMissingInformation();
      return;
    } else if (txtPassword != txtConfirmPassword) {
      this.onPasswordDoesNotMatch();
      return;
    }

    const birthYear = skip_bday === true ? "" : value;

    this.props.dispatch(
      userEdit(
        this.props.user.token,
        txtUserName,
        txtPassword,
        gender,
        birthYear,
        this.onErrorRegistrationCallback
      )
    );
  };

  onMissingInformation = () => {
    this.dialogbox.alert(
      lang.error.title.edit_profile,
      lang.error.desc.missing_information
    );
  };

  onErrorRegistrationCallback = message => {
    if (message) {
      this.dialogbox.alert(lang.error.title.edit_profile, message);
    }
  };

  onPasswordDoesNotMatch = () => {
    this.dialogbox.alert(
      lang.error.title.edit_profile,
      lang.error.desc.password_not_match
    );
  };

  onBackClick = () => {
    Actions.pop();
  };

  onUpdateUserNameValue = text => {
    this.setState({
      txtUserName: text
    });
  };

  onUpdatePasswordValue = text => {
    this.setState({
      txtPassword: text
    });
  };

  onUpdateConfirmPasswordValue = text => {
    this.setState({
      txtConfirmPassword: text
    });
  };

  onUpdatePasswordValue = text => {
    this.setState({
      txtPassword: text
    });
  };

  onSelectGender = sexPref => {
    let sexData = [false, false, false];
    sexData[sexPref] = true;

    this.setState({ sexData });
  };

  render() {
    let { txtUserName, txtPassword, txtConfirmPassword } = this.state;

    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <KeyboardAwareScrollView
          style={styles.container}
          contentContainerStyle={{ paddingBottom: 60 }}
          behavior="padding"
          extraScrollHeight={64}
          keyboardShouldPersistTaps="handled"
        >
          <Crown
            title={lang.crown_title.rediger_profil}
            crownType={constants.crownType.yellow}
          />

          <View style={{ marginTop: 16 }}>
            <View style={{ marginTop: 16 }}>
              <Text style={[styles.txtInfoTitle]}>
                {lang.account.update_user_name}
              </Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdateUserNameValue}
                  placeHolder={lang.account.enter_user_name}
                  textInput={txtUserName}
                />
              </View>
            </View>

            <View style={{ marginTop: 40 }}>
              <Text style={[styles.txtInfoTitle]}>
                {lang.account.update_password}
              </Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdatePasswordValue}
                  placeHolder={lang.account.enter_password}
                  textInput={txtPassword}
                />
              </View>
            </View>

            <View style={{ marginTop: 16 }}>
              <Text style={[styles.txtInfoTitle]}>
                {lang.account.repeat_password}
              </Text>

              <View style={{ marginTop: 8 }}>
                <BkTextInput
                  onUpdateInputValue={this.onUpdateConfirmPasswordValue}
                  placeHolder={lang.account.enter_password}
                  textInput={txtConfirmPassword}
                />
              </View>
            </View>
          </View>
          <View style={{ marginTop: 16, width: "95%" }}>
            <Text style={[styles.txtInfoTitle]}>{lang.account.sex}</Text>

            <View
              style={{
                marginTop: 8,
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  flex: 2,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Text style={[styles.txtRadioTitle]}>{lang.account.man}</Text>

                <BkRadioButton
                  selected={this.state.sexData[0]}
                  size={20}
                  onActionClick={() => {
                    this.onSelectGender(0);
                  }}
                  theme={"#f8b016"}
                />
              </View>
              <View
                style={{
                  flex: 2,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Text style={[styles.txtRadioTitle]}>{lang.account.woman}</Text>

                <BkRadioButton
                  selected={this.state.sexData[1]}
                  size={20}
                  onActionClick={() => {
                    this.onSelectGender(1);
                  }}
                  theme={"#f8b016"}
                />
              </View>
              <View
                style={{
                  flex: 2,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Text style={[styles.txtRadioTitle]}>
                  {lang.account.dont_inform}
                </Text>

                <BkRadioButton
                  selected={this.state.sexData[2]}
                  size={20}
                  onActionClick={() => {
                    this.onSelectGender(2);
                  }}
                  theme={"#f8b016"}
                />
              </View>
            </View>
          </View>
          <View style={{ marginTop: 16 }}>
            <Text style={[styles.txtInfoTitle]}>
              {lang.account.year_of_birth}
            </Text>

            <View
              style={{
                marginTop: 8,
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <View style={{ flex: 1, paddingRight: 20 }}>
                <BKDropDown
                  dataParams={this.state.yearsBetween}
                  value={this.state.value}
                  fontStyle={styles.txtDropdown}
                  onChange={value => {
                    this.setState({ value, skip_bday: false });
                  }}
                />
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Text style={[styles.txtRadioTitle]}>
                  {lang.account.dont_inform}
                </Text>

                <BkRadioButton
                  selected={this.state.skip_bday}
                  size={20}
                  onActionClick={() => {
                    const skip_bday = !this.state.skip_bday;
                    this.setState({ skip_bday });
                  }}
                  theme={"#f8b016"}
                />
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <Footer hasDone={true} onDoneClick={this.onDoneClick} />
        <ActionBar showBack={true} onBackClick={this.onBackClick} />
        <DialogBox
          ref={ref => {
            this.dialogbox = ref;
          }}
        />
        <Loading isVisible={this.props.isLoading} />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.user.isLoggedIn,
    user: state.user.user,
    error: state.user.error,
    isLoading: state.ui.isLoading
  };
};

export default connect(mapStateToProps)(EditProfile);

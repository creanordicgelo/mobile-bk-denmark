import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 32,
    paddingLeft: 24,
    paddingRight: 24,
    marginBottom: 88
  },
  imgMenu: {
    height: 40,
    width: 100,
    justifyContent: "center",
    alignItems: "center"
  },
  txtTitle: {
    color: "#e2a271",
    fontSize: 24,
    fontWeight: "bold"
  },
  txtInfoTitle: {
    color: "#f8b016",
    fontSize: 12,
    fontWeight: "bold"
  },
  txtRadioTitle: {
    color: "#f8b016",
    fontSize: 10,
    marginRight: 10
  },
  txtInfo: {
    color: "#404042",
    fontSize: 24
  },
  txtDescription: {
    color: "gray",
    fontSize: 12
  }
});

export default styles;

import React from "react";
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  RefreshControl,
  FlatList,
  ScrollView,
  BackHandler
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { isIphoneX } from "react-native-iphone-x-helper";
import ImageLoad from "react-native-image-placeholder";
import DialogBox from "react-native-dialogbox";

import styles from "./styles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import BKProfile from "../../common/components/BKProfile/BKProfile";
import Loading from "../../common/components/Loading/Loading";

import lang from "../../config/strings.json";
import {
  getCoupons,
  getClaimedCoupons,
  getCouponsLoggedIn
} from "../../actions/coupons";
import { updateLoginLocation } from "../../actions/user";

class Coupons extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      refreshing: false
    };
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      Actions.replace("home");
      return true;
    });

    const { dispatch, isLoggedIn, user } = this.props;
    if (isLoggedIn) {
      dispatch(getCouponsLoggedIn(user.token));
    } else {
      dispatch(getCoupons());
    }
    dispatch(getClaimedCoupons());
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onRefresh = () => {
    this.setState({ refreshing: true });

    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 300);
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 400;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  registerUser = () => {
    this.props.dispatch(updateLoginLocation("coupons"));
    Actions.login();
  };

  onNonMemberClicked = () => {
    this.dialogbox.alert("", lang.error.non_member_clicked);
  };

  render() {
    const { isLoggedIn } = this.props;
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop: isIphoneX() === true ? 40 : 20
        }}
        source={require("../../images/bg.png")}
      >
        {!isLoggedIn && (
          <View
            style={{
              width: "100%",
              height: 130,
              marginTop: 40,
              alignItems: "center",
              justifyContent: "center",
              paddingHorizontal: 20
            }}
          >
            <BKProfile isLoggedIn={isLoggedIn} onPress={this.registerUser} />
          </View>
        )}
        <ScrollView
          scrollEventThrottle={400}
          style={{ paddingTop: isLoggedIn === true ? 20 : 0 }}
          onScroll={({ nativeEvent }) => {
            if (this.isCloseToBottom(nativeEvent)) {
              // get other deatails for pagination
            }
          }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.onRefresh}
            />
          }
        >
          <View style={[styles.container, { paddingTop: isLoggedIn ? 32 : 0 }]}>
            <FlatList
              data={this.props.coupons.data}
              showsVerticalScrollIndicator={false}
              renderItem={({ item, index }) => (
                <TouchableOpacity
                  style={{
                    marginTop: 16,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  onPress={() => {
                    if (item.MembersOnly === true && !isLoggedIn) {
                      this.onNonMemberClicked();
                    } else {
                      Actions.couponDetail({ couponDetail: item });
                    }
                  }}
                >
                  <ImageLoad
                    resizeMode="stretch"
                    loadingStyle={{ size: "small", color: "black" }}
                    style={styles.imgFullWidth}
                    source={{
                      uri: item.Thumbnail
                    }}
                  />
                </TouchableOpacity>
              )}
              keyExtractor={item => item._id.toString()}
            />
          </View>
        </ScrollView>
        <ActionBar />
        <DialogBox
          ref={ref => {
            this.dialogbox = ref;
          }}
        />
        <Loading isVisible={this.props.isLoading} />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    coupons: state.coupons,
    isLoading: state.ui.isLoading,
    isLoggedIn: state.user.isLoggedIn,
    user: state.user.user
  };
};

export default connect(mapStateToProps)(Coupons);

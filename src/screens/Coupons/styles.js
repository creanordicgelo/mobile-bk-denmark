import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 32,
    justifyContent: "center",
    alignItems: "center"
  },
  imgFullWidth: {
    width: widthDimension * 0.9,
    height: widthDimension * 0.5
  }
});

export default styles;

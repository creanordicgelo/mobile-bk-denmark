import React from "react";
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  Platform,
  TextInput,
  Dimensions,
  TouchableHighlight,
  Linking
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { Container, Text, Button, Icon } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from "react-native-maps";
import * as Animatable from "react-native-animatable";
import DialogBox from "react-native-dialogbox";
import * as _ from "lodash";

import styles from "./styles";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import Loading from "../../common/components/Loading/Loading";
import textStyles from "../../common/values/textStyles";
import { getRestaurants } from "../../actions/restaurants";
import renderIf from "../../common/utils/renderIf";

import lang from "../../config/strings.json";

const screenSize = Dimensions.get("window");

class Delivery extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: "",
      currentShownCallOut: "",
      showHideMarker: false,
      longDelta: 0,
      latDelta: 0,
      tracksViewChanges: false,
      restaurants: props.restaurants
    };
    this.marker = {};
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.restaurants.length === 0 &&
      nextProps.restaurants.length !== 0
    ) {
      this.zoomToDenmark();

      this.setState({ restaurants: nextProps.restaurants });
    } else if (this.props.restaurant != nextProps.restaurant) {
      this.setState({ restaurants: nextProps.restaurants });
    }
  }

  zoomToDenmark = () => {
    // Animate Center Mark Denmark
    this.mapRef.animateCamera({
      center: {
        latitude: 56.2639,
        longitude: 9.5018
      },
      zoom: 6
    });
  };

  componentDidMount() {
    this.props.dispatch(getRestaurants());

    if (this.props.restaurants.length !== 0) {
      const markers = this.props.restaurants.map(restaurant => restaurant.Name);

      setTimeout(() => {
        this.zoomToDenmark();
      }, 600);
    }
  }

  zoomToSpecifiedMarker = index => {
    const targetRestaurant = this.state.restaurants[index];
    const { longDelta, latDelta } = this.state;
    const absoluteDelta = Math.floor(latDelta);
    let additional = 0.25;

    if (absoluteDelta === 4) {
      additional = 1;
    } else if (absoluteDelta === 3 || absoluteDelta === 2) {
      additional = 0.5;
    }

    this.mapRef.animateCamera({
      center: {
        latitude: +targetRestaurant.Latitude + additional,
        longitude: +targetRestaurant.Longitude
      },
      zoom: 7
    });

    this.setState({ currentShownCallOut: targetRestaurant.Name });
  };

  searchRestaurant = () => {
    if (!_.isEmpty(this.state.currentShownCallOut)) {
      this.marker[this.state.currentShownCallOut].hideCallout();
      this.setState({ showHideMarker: false });
    }

    if (_.isEmpty(this.state.searchText)) {
      return;
    }

    const targetRestaurants = _.filter(this.props.restaurants, restaurant => {
      const hasMatchByName = restaurant.Name.toLowerCase().startsWith(
        this.state.searchText.toLowerCase()
      );
      const hasMatchByAddress = restaurant.Address.toLowerCase().includes(
        this.state.searchText.toLowerCase()
      );

      return hasMatchByName || hasMatchByAddress;
    });

    const targetMarkers = targetRestaurants.map(restaurant => restaurant.Name);
    if (targetMarkers.length !== 0) {
      let object = {};
      if (targetMarkers.length === 1) {
        this.marker[targetRestaurants[0].Name].showCallout();
        object.currentShownCallOut = targetRestaurants[0].Name;
      } else {
        object.restaurants = targetRestaurants;
        this.marker = {};
      }

      this.setState(object);

      this.mapRef.fitToSuppliedMarkers(targetMarkers, true);
    } else {
      this.onShowNoMarkers();
      this.setState({ restaurants: this.props.restaurants });
    }
  };

  onShowNoMarkers = () => {
    this.dialogbox
      .alert(lang.error.title.search_map, lang.error.error_search_map)
      .then(() => this.zoomToDenmark());
  };

  renderOpening = (opening, index) => {
    return (
      <View
        key={index}
        style={{
          width: "100%",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <Text
          style={[
            {
              color: "white",
              fontSize: 12,
              textAlign: "left",
              flex: 1
            }
          ]}
        >
          {opening.day}
        </Text>
        <Text
          key={index}
          style={[
            {
              color: "white",
              fontSize: 12,
              textAlign: "right",
              flex: 1
            }
          ]}
        >
          {opening.time}
        </Text>
      </View>
    );
  };

  onPressCallOut = restaurant => {
    const directionURL = `https://www.google.com/maps/search/?api=1&query=Burger King,${
      restaurant.Address
    }`;

    Linking.openURL(directionURL).catch(err =>
      console.error("An error occurred", err)
    );
  };

  showMarker = (restaurant, index) => {
    return (
      <Marker
        key={`${restaurant.ID}-${index}`}
        ref={ref => {
          this.marker[restaurant.Name] = ref;
        }}
        coordinate={{
          latitude: +restaurant.Latitude,
          longitude: +restaurant.Longitude
        }}
        onPress={e => {
          this.zoomToSpecifiedMarker(index);

          if (this.state.showHideMarker === false) {
            this.setState({ showHideMarker: true }, () => {
              this.refs.view.fadeIn(1000);
            });
          }
        }}
        title={restaurant.Name}
        identifier={restaurant.Name}
        image={require("../../images/mini_logo.png")}
      >
        <Callout
          onPress={evt => {
            this.onPressCallOut(restaurant);
          }}
          tooltip={true}
        >
          <View>
            <View
              style={{
                alignItems: "center",
                paddingTop: 20,
                paddingBottom: 20,
                paddingHorizontal: 30,
                backgroundColor: "#e92625",
                borderRadius: 25,
                zIndex: 0
              }}
            >
              <Text
                style={[
                  textStyles.txtCalloutStyle,
                  {
                    fontSize: 14,
                    color: "white",
                    textAlign: "center",
                    marginBottom: 10
                  }
                ]}
              >
                {`BURGER KING ${restaurant.Name.toUpperCase()}`}
              </Text>
              <Text
                style={[
                  {
                    color: "white",
                    fontSize: 12,
                    textAlign: "center",
                    marginBottom: 10
                  }
                ]}
              >
                {restaurant.Address}
              </Text>
              <View>
                {restaurant.openingHourArray.map((opening, index) =>
                  this.renderOpening(opening, index)
                )}
              </View>
              <Text
                style={[
                  {
                    color: "white",
                    fontSize: 12,
                    textAlign: "center",
                    marginTop: 10,
                    marginBottom: 10
                  }
                ]}
              >
                {`${lang.restaurant.telephone} : ${restaurant.Telephone}`}
              </Text>

              <Text
                style={[
                  {
                    color: "white",
                    fontSize: 12,
                    textAlign: "center",
                    marginBottom: 20,
                    textDecorationLine: "underline"
                  }
                ]}
              >
                {lang.restaurant.click_direction}
              </Text>
            </View>
            <View
              style={{
                height: 20,
                alignItems: "center"
              }}
            >
              <View style={styles.triangle} />

              <View
                style={{
                  height: 40,
                  width: 40,
                  borderRadius: 20,
                  backgroundColor: "#e92625",
                  top: -21,
                  position: "absolute"
                }}
              />
            </View>
          </View>
        </Callout>
      </Marker>
    );
  };

  regionChangeComplete = details => {
    const longDelta = details.longitudeDelta;
    const latDelta = details.latitudeDelta;

    this.setState({ longDelta, latDelta });
  };

  render() {
    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop:
            isIphoneX() === true ? 100 : Platform.OS === "ios" ? 80 : 72
        }}
        source={require("../../images/bg.png")}
      >
        <MapView
          provider={PROVIDER_GOOGLE}
          ref={ref => {
            this.mapRef = ref;
          }}
          style={{ height: "100%", width: "100%" }}
          zoomEnabled={true}
          loadingEnabled={true}
          onRegionChangeComplete={this.regionChangeComplete}
          initialCamera={{
            center: {
              latitude: 56.2639,
              longitude: 9.5018
            },
            pitch: 1,
            heading: 1,
            altitude: 6,
            zoom: 3
          }}
        >
          {this.state.restaurants.map((restaurant, index) =>
            this.showMarker(restaurant, index)
          )}
        </MapView>

        <View
          style={{
            flexDirection: "row",
            position: "absolute",
            width: "100%",
            top: 110,
            height: 50,
            paddingHorizontal: 24
          }}
        >
          <View
            style={{
              flex: 8,
              backgroundColor: "white",
              borderBottomLeftRadius: 5,
              borderTopLeftRadius: 5
            }}
          >
            <TextInput
              style={[
                textStyles.txtSubMenuHeaderTitle,
                { flex: 1, color: "black" }
              ]}
              placeholder={"SØG"}
              value={this.state.searchText}
              onChangeText={text => {
                let object = { searchText: text };
                let willZoom = () => {};

                if (!_.isEmpty(this.state.currentShownCallOut)) {
                  this.marker[this.state.currentShownCallOut].hideCallout();
                  object.showHideMarker = false;
                }

                if (_.isEmpty(text)) {
                  willZoom = () => {
                    this.zoomToDenmark();
                  };
                  object.restaurants = this.props.restaurants;
                }

                this.setState(object, willZoom);
              }}
            />
          </View>
          <TouchableOpacity
            style={{
              backgroundColor: "green",
              flex: 2,
              alignItems: "center",
              borderBottomRightRadius: 5,
              borderTopRightRadius: 5,
              justifyContent: "center",
              zIndex: 0
            }}
            onPress={this.searchRestaurant}
          >
            <Icon name="search" style={{ fontSize: 18, color: "white" }} />
          </TouchableOpacity>
        </View>
        {renderIf(
          this.state.showHideMarker === true,
          <Animatable.View
            style={{
              position: "absolute",
              width: 50,
              height: 50,
              borderRadius: 25,
              bottom: 50,
              right: 20,
              backgroundColor: "#e92625"
            }}
            ref="view"
          >
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
              onPress={() => {
                if (!_.isNull(this.state.currentShownCallOut)) {
                  this.marker[this.state.currentShownCallOut].hideCallout();
                  this.setState({
                    showHideMarker: false,
                    currentShownCallOut: ""
                  });
                }
              }}
            >
              <Icon name="close" style={{ color: "white" }} />
            </TouchableOpacity>
          </Animatable.View>
        )}
        <ActionBar />
        <DialogBox
          ref={ref => {
            this.dialogbox = ref;
          }}
        />
        <Loading isVisible={this.props.isLoading} />
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoading: state.ui.isLoading,
    restaurants: state.restaurants.locations
  };
};

export default connect(mapStateToProps)(Delivery);

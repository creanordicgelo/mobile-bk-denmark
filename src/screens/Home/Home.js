import React from "react";
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  ScrollView,
  Platform,
  TouchableWithoutFeedback,
  Linking,
  Modal
} from "react-native";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import { Container, Text, Button } from "native-base";
import { isIphoneX } from "react-native-iphone-x-helper";
import * as Animatable from "react-native-animatable";
import firebase from "react-native-firebase";
import DialogBox from "react-native-dialogbox";
import SInfo from "react-native-sensitive-info";
import { isUndefined, isNull } from "lodash";

import styles from "./styles";
import { WebView } from "react-native-webview";
import { updateLoginLocation } from "../../actions/user";
import { getDeliveryUrl } from "../../actions/restaurants";
import { registerPushNotification } from "../../actions/pushNotif";
import ActionBar from "../../common/components/ActionBar/ActionBar";
import textStyles from "../../common/values/textStyles";
import renderIf from "../../common/utils/renderIf";

import {
  PROD_URL,
  IS_STAGING_ENVIRONMENT,
  AGREED_TO_GDPR,
  USER_DATA,
  CLAIMED_COUPONS
} from "../../common/values/constants";

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showMenu: false,
      modalOpen: false,
      link: ""
    };

    this.viewMenu = [];
  }

  componentWillMount() {
    firebase
      .messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          // user has permissions
        } else {
          // user doesn't have permission
          firebase
            .messaging()
            .requestPermission()
            .then(() => {
              // User has authorised
            })
            .catch(error => {
              // User has rejected permissions
            });
        }
      });

    firebase
      .messaging()
      .getToken()
      .then(fcmToken => {
        if (fcmToken) {
          // user has a device token
          registerPushNotification(fcmToken);
        } else {
          // user doesn't have a device token yet
        }
      });
  }

  componentDidMount() {
    setTimeout(() => {
      let delay = 1000;

      this.setState({ showMenu: true }, () => {
        this.viewMenu.forEach((viewMenu, index) => {
          viewMenu.bounceInDown(delay);
          delay += 200;
        });
      });
    }, 200);
  }

  openLink = url => {
    this.setState({ modalOpen: true, link: url });
  };

  redirectLink = url => {
    Linking.openURL(url).catch(err => console.error("An error occurred", err));
  };

  switchServer = () => {
    const { isProduction } = this.props;
    const serverText = isProduction === true ? "Production" : "Staging";
    this.dialogbox.confirm({
      content: `You are in ${serverText} server do you want to switch server?`,
      ok: {
        callback: () => {
          let serverSwitch = isProduction ? "true" : "false";

          SInfo.setItem(IS_STAGING_ENVIRONMENT, serverSwitch, {}).then(
            value => {
              SInfo.deleteItem(AGREED_TO_GDPR, {});
              SInfo.deleteItem(USER_DATA, {});
              SInfo.deleteItem(CLAIMED_COUPONS, {});
              Actions.replace("splash");
            }
          );
        }
      }
    });
  };

  onBackClick = () => {
    this.setState({ modalOpen: false });
  };

  render() {
    const {
      feedback,
      delivery,
      eventlink,
      julekalender,
      gamelink,
      worldgourmet,
      yellandwin
    } = this.props.links;

    const willShowGameLink = !isUndefined(gamelink) && !isNull(gamelink);
    const willShowCalendarButton =
      !isUndefined(julekalender) && !isNull(julekalender);
    const willShowEventLink = !isUndefined(eventlink) && !isNull(eventlink);
    const willShowYellAndWin = !isUndefined(yellandwin) && !isNull(yellandwin);

    return (
      <ImageBackground
        style={{
          flex: 1,
          paddingTop:
            isIphoneX() === true ? 100 : Platform.OS === "ios" ? 50 : 30
        }}
        source={require("../../images/bg.png")}
      >
        <ScrollView
          style={{
            flex: 0.85
          }}
        >
          <View
            style={{
              flex: 1,
              opacity: this.state.showMenu === false ? 0 : 1,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            {willShowYellAndWin && (
              <TouchableOpacity
                onPress={() => {
                  if (yellandwin) {
                    this.redirectLink(yellandwin);
                  }
                }}
              >
                <Animatable.View
                  ref={ref => {
                    this.viewMenu[8] = ref;
                  }}
                >
                  <Image
                    resizeMode={"cover"}
                    style={[styles.imgYellAndWin]}
                    source={require("../../images/yellandwin.jpg")}
                  />
                </Animatable.View>
              </TouchableOpacity>
            )}

            {willShowEventLink && (
              <TouchableOpacity
                onPress={() => {
                  if (eventlink) {
                    this.openLink(eventlink);
                  }
                }}
              >
                <Animatable.View
                  ref={ref => {
                    this.viewMenu[8] = ref;
                  }}
                >
                  <Image
                    resizeMode={"cover"}
                    style={[styles.imgRoulette]}
                    source={require("../../images/img_roulette.png")}
                  />
                </Animatable.View>
              </TouchableOpacity>
            )}
            {willShowGameLink && (
              <TouchableOpacity
                onPress={() => {
                  if (gamelink) {
                    this.openLink(gamelink);
                  }
                }}
              >
                <Animatable.View
                  ref={ref => {
                    this.viewMenu[8] = ref;
                  }}
                >
                  <Image
                    resizeMode={"contain"}
                    style={[styles.imgCalendar]}
                    source={require("../../images/img_summer_roulete.png")}
                  />
                </Animatable.View>
              </TouchableOpacity>
            )}
            {willShowCalendarButton && (
              <TouchableOpacity
                onPress={() => {
                  if (julekalender) {
                    this.openLink(julekalender);
                  }
                }}
              >
                <Animatable.View
                  ref={ref => {
                    this.viewMenu[8] = ref;
                  }}
                >
                  <Image
                    resizeMode={"cover"}
                    style={[styles.imgCalendar]}
                    source={require("../../images/img_newevent.png")}
                  />
                </Animatable.View>
              </TouchableOpacity>
            )}
            <TouchableOpacity
              onPress={() => {
                Actions.coupons();
              }}
            >
              <Animatable.View
                ref={ref => {
                  this.viewMenu[7] = ref;
                }}
              >
                <Image
                  resizeMode={"cover"}
                  style={[styles.imgMenu, { marginBottom: 5 }]}
                  source={require("../../images/img_coupon.png")}
                />
              </Animatable.View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                if (worldgourmet) {
                  this.openLink(worldgourmet);
                }
              }}
            >
              <Animatable.View
                ref={ref => {
                  this.viewMenu[6] = ref;
                }}
              >
                <Image
                  resizeMode={"cover"}
                  style={[styles.imgMenu, { marginBottom: 5 }]}
                  source={require("../../images/img_world_gourmet.png")}
                />
              </Animatable.View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                Actions.campaign();
              }}
            >
              <Animatable.View
                ref={ref => {
                  this.viewMenu[5] = ref;
                }}
              >
                <Image
                  resizeMode={"contain"}
                  style={styles.imgCampaign}
                  source={require("../../images/img_campaign.png")}
                />
              </Animatable.View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                Actions.restaurant();
              }}
            >
              <Animatable.View
                ref={ref => {
                  this.viewMenu[4] = ref;
                }}
              >
                <Image
                  resizeMode={"contain"}
                  style={[styles.imgMenu, { marginBottom: 5 }]}
                  source={require("../../images/img_restaurant.png")}
                />
              </Animatable.View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                Actions.menu();
              }}
            >
              <Animatable.View
                ref={ref => {
                  this.viewMenu[3] = ref;
                }}
              >
                <Image
                  resizeMode={"contain"}
                  style={[styles.imgMenu, { marginBottom: 5 }]}
                  source={require("../../images/img_menu.png")}
                />
              </Animatable.View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                if (feedback) {
                  this.redirectLink(feedback);
                }
              }}
            >
              <Animatable.View
                ref={ref => {
                  this.viewMenu[2] = ref;
                }}
              >
                <Image
                  resizeMode={"contain"}
                  style={[styles.imgMenu, { marginBottom: 3 }]}
                  source={require("../../images/img_feedback.png")}
                />
              </Animatable.View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                if (delivery) {
                  this.redirectLink(delivery);
                }
              }}
            >
              <Animatable.View
                ref={ref => {
                  this.viewMenu[1] = ref;
                }}
              >
                <Image
                  resizeMode={"contain"}
                  style={[styles.imgMenu]}
                  source={require("../../images/img_delivery.png")}
                />
              </Animatable.View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                if (this.props.isLoggedIn === true) {
                  Actions.profileInfo();
                } else {
                  this.props.dispatch(updateLoginLocation("home"));
                  Actions.login();
                }
              }}
            >
              <Animatable.View
                ref={ref => {
                  this.viewMenu[0] = ref;
                }}
              >
                <Image
                  resizeMode={"contain"}
                  style={[styles.imgMenu]}
                  source={require("../../images/img_my_bk.png")}
                />
              </Animatable.View>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <View
          style={{
            flex: 0.15,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <TouchableWithoutFeedback
            delayLongPress={15000}
            onLongPress={this.switchServer}
          >
            <Image
              style={styles.imgSlogan}
              source={require("../../images/img_slogan.png")}
            />
          </TouchableWithoutFeedback>
        </View>
        <DialogBox
          ref={ref => {
            this.dialogbox = ref;
          }}
        />
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalOpen}
        >
          <ImageBackground
            style={{ flex: 1 }}
            source={require("../../images/bg.png")}
          >
            <View
              style={{ backgroundColor: "transparent", flex: 1, marginTop: 80 }}
            >
              <WebView
                style={{ flex: 1 }}
                source={{
                  uri: this.state.link
                }}
              />
            </View>
            <ActionBar showBack={true} onBackClick={this.onBackClick} />
          </ImageBackground>
        </Modal>
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.user.isLoggedIn,
    data: state.reducer.data,
    ui: state.ui,
    links: state.links.links,
    isProduction: state.config.baseURL === PROD_URL
  };
};

export default connect(mapStateToProps)(Home);

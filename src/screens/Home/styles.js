import { StyleSheet, Platform, Dimensions } from "react-native";

const widthDimension = Dimensions.get("window").width;

const imgMenuWidth = widthDimension * 0.7;
const unitMenu = imgMenuWidth / 26;
const imgMenuHeight = unitMenu * 7;

const unitMenuCampaign = imgMenuWidth / 65;
const imgMenuCampaignHeight = unitMenuCampaign * 22;

const unitCalendar = imgMenuWidth / 227;
const calendarHeight = unitCalendar * 83;

const unitMenuRoulettee = imgMenuWidth / 65;
const imgMenuRpouletteHeight = unitMenuRoulettee * 22;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center"
  },
  imgCalendar: {
    width: imgMenuWidth,
    height: calendarHeight
  },
  imgMenu: {
    width: imgMenuWidth,
    height: imgMenuHeight
  },
  imgCampaign: {
    width: imgMenuWidth,
    height: imgMenuCampaignHeight
  },
  imgSlogan: {
    width: widthDimension * 0.55,
    height: (widthDimension * 0.55) / 4
  },
  imgRoulette: {
    width: imgMenuWidth * 1.05,
    height: imgMenuHeight
  },
  imgYellAndWin: {
    width: imgMenuWidth * 1.00,
    height: imgMenuHeight,
    marginBottom: 5,
  }
});

export default styles;
